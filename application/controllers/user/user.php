<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class User extends Home_Controller
{
	function __construct()
 	{
   		parent::__construct();
	}

	public function index()
	{
		$data['profile'] = $this->user_model->get_profile();
		$this->load->view('user/home' ,$data);
	}

	public function pages($page = 'home',$a='',$id='')
	{
		if($a=='web')
		{
			if(!file_exists(APPPATH.'/views/user/'.$page.'.php'))
			{
				// Whoops, we don't have a page for that!
				show_404();
			}
			$data['profile'] = $this->user_model->get_profile();
			$data['products'] = $this->admin_model->get_products();
			$data['company'] = $this->admin_model->get_company();
			$this->load->view('user/'.$page ,$data);
		}
	}

	public function update_profile($a='')
	{
		$res=$this->user_model->update_profile($a);
		if($a=='web')
		{
			if($res==1)
			{
				$data['profile'] = $this->user_model->get_profile();
				$this->load->view('user/profile' ,$data);
			}
		}
		else if($a=='app')
		{
			if($res['ProfileUpdateFlag']==1)
			{
				$siteresponse['ProfileUpdateFlag']=$res['ProfileUpdateFlag'];
				$siteresponse['ErrorFlag']=$res['ErrorFlag'];
				$siteresponse['TokenFlag']=$res['TokenFlag'];
				$PKUserID=$_POST['PKUserID'];
				$r=$this->admin_model->get_updated_user($PKUserID);
				$siteresponse['Contacts']=$r;
				echo json_encode($siteresponse);
			}
		}
	}

	public function update_password($a='')
	{
		$res=$this->user_model->update_password($a);
		if($a=='web')
		{
			if($res==1)
			{
				$data['profile'] = $this->user_model->get_profile();
				$this->load->view('user/profile' ,$data);
			}
		}
		else if($a=='app')
		{
			echo json_encode($res);
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */