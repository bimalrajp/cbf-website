<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Chat extends Home_Controller
{
	function __construct()
 	{
   		parent::__construct();
	}

	public function msg($page='send',$a='web',$id='')
	{
		if($a=='web')
		{
			if(!file_exists(APPPATH.'/views/user/'.$page.'.php'))
			{
				// Whoops, we don't have a page for that!
				show_404();
			}
			$data['profile'] = $this->user_model->get_profile();
			$data['products'] = $this->admin_model->get_products();
			$data['company'] = $this->admin_model->get_company();
			$this->load->view('user/'.$page ,$data);
		}
		else if($a=='app')
		{
			if($page=='send')
			{
				$res=$this->chat_model->send_msg();
				echo json_encode($res);
			}
			else if($page=='getkey')
			{
				$res=$this->chat_model->get_server_key();
				echo json_encode($res);
			}
			else if($page=='get')
			{
				$res=$this->chat_model->get_msgs();
				echo json_encode($res);
			}
		}
	}

	public function update_profile($id='')
	{
		$res=$this->user_model->update_profile($id);
		if($res==1)
		{
			$data['profile'] = $this->user_model->get_profile();
			$this->load->view('user/profile' ,$data);
		}
	}
}