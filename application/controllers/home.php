<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Home extends Home_Controller
{
	function __construct()
 	{
   		parent::__construct();
	}

	public function index()
	{
		$data['directory']=$this->user_model->get_directory();
		$data['news'] = $this->admin_model->get_news();
		$this->load->view('pages/home',$data);
	}

	public function view($page = 'pages/home',$id='')
	{
		if(!file_exists(APPPATH.'/views/pages/'.$page.'.php'))
		{
			//Whoops, we don't have a page for that!
			show_404();
		}  
		if($page=='register')
		{
			$data['companies']=$this->admin_model->get_company();
			$data['products']=$this->admin_model->get_products();
		}  
		if($page=='userdetail')
		{
			$data['profile'] = $this->user_model->get_profile($id);
		}
		else
		{
			$data['gallery']=$this->gallery_model->get_gallery();
			$data['companies']=$this->admin_model->get_company();
			$data['directory']=$this->user_model->get_directory();
			$data['document']=$this->document_model->get_document();
			$data['documentdoc'] = $this->document_model->get_documentdoc();
			$data['news'] = $this->admin_model->get_news();
	    }
		if($page=='gallery1'&&$id!='')
		{
			
			$data['gallery'] = $this->gallery_model->get_galleryimgsbyid($id);
			//$this->load->view('pages/'.$page,$data);
		}
		$this->load->view('pages/'.$page,$data);
	}

	public function register($a="web")
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('RUserName', 'RUserName', 'trim|xss_clean|required|is_unique[user.UserName]');
		
		if ($this->form_validation->run()===FALSE)
		{
			$_SESSION["message"]="This Username is already registered. Please try again..!!";
			redirect(base_url()."register", 'refresh');
		}
		else
		{
			if($a=="web")
			{
				$r=$this->user_model->set_user();
				if($r==1)
				{
					$_SESSION["message"]="User registeration Successful..!! Wait for ADMIN's approval..!!";
					redirect(base_url().'register', 'refresh');
					//redirect(base_url().'user/home', 'refresh');
				}
				else
				{
					$_SESSION["message"]="User registered Unsuccessful!";
					redirect(base_url().'register', 'refresh');
				}
			}
			else if($a=="app")
			{
				$r=$this->user_model->set_user();
				if($r==1)
				{
					$this->verifylogin($a, 1);
				}
				else
				{
					$logindetails['RegisterFlag']=0;
					echo $data['siteresponse']=json_encode($logindetails);
				}
			}
		}
	}
	
	public function search($a='web')
	{
		if($a=='web')
		{
			$data['directory']=$this->admin_model->search($a);
			$this->load->view('pages/directory' ,$data);
		}
		if($a=='app')
		{
			$res=$this->admin_model->search($a);
			echo json_encode($res);
		}
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */