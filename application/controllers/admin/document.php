<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Document extends CI_Controller {



	function __construct()
 	{
   		parent::__construct();
		/*if (!$this->session->userdata('logged_in'))
        { 
            redirect('admin', 'refresh');
        }*/
		$this->load->helper(array('form', 'url'));
		$this->load->model('document_model');
 	}
	
	
	
	public function index($a='document',$c='')
	{
		/*$this->load->library('form_validation');
		$session_data=$this->session->userdata('logged_in');
	    $data['username']=$session_data['username'];
		$data['error']=$c;*/
	    if($a=='document')
		{
			$data['document']=$this->document_model->get_document();
			$this->load->view('admin/document', $data);
		}
		else if($a=='adddocument')
		{
			$this->load->view('admin/adddocument');
		}
		else if($a=='adddocumentdoc')
		{
			$data['documentid']=$c;
			$this->load->view('admin/adddocumentdoc', $data);
		}
		else if($a=='documentview')
		{
			$gname=$this->document_model->get_documentbyid($c);
			$data['did'] =$c;
			$data['document'] = $this->document_model->get_documentdocbyid($c);
			$this->load->view('admin/documentview', $data);
		}
		else if($a=='edit')
		{
			$data['document']=$this->document_model->get_documentbyid($c);
			$this->load->view('admin/editdocument',$data);
		}
	}
	
	
	
	public function create($a="",$id="")
	{
	 /* $this->gallery_model->set_gallery();
	  $r=$data['gallery']=$this->gallery_model->get_gallery();
	  $this->load->view('admin/gallery', $data);*/
	  
	  if($a=='adddocument')
	  	{
			$r=$this->document_model->set_document();
			if($r==1)
			{
		  		redirect(base_url().'admin/document', 'refresh');
				/*$data['document']=$this->document_model->get_document();
				$this->load->view('admin/document', $data);*/
		 	}
		}
	 if($a=='adddocumentdoc')
		{
			$r=$this->document_model->set_documentdoc($id);
			if($r==1)
			{
				redirect('admin/document/documentview/'.$id, 'refresh');
			}
		}
	  
	  
	  
	}
	

public function delete($id='',$did='')
	{
		$path='./assets/upload_documentdoc/';
		$pathgi='';
		/*$dname=$this->document_model->get_documentbyid($did);
		foreach ($dname as $gn):
		$pathgi='./assets/upload_document/'.$gn['Logo'];
		endforeach;*/
		if($did!='')
		{
			$iname=$this->document_model->get_documentdocbyid($did);
			foreach ($iname as $in):
			$path=$path.$in['Doc'];
			endforeach;
			$this->document_model->dlt_documentdoc($path,$did);
			redirect('admin/document/documentview/'.$id, 'refresh');
			//$this->index('galleryview',$gid);
		}
		else
		{
			$this->document_model->dlt_document($id);
			redirect('admin/document', 'refresh');
		}
		
	}

/*public function edit($gid='')
	{
	$data['document']=$this->document_model->get_documentbyid($gid);
	$this->load->view('admin/editdocument',$data);
	
	}*/
	
public function editdocument($did='')
	{
	$r=$this->document_model->edit_document($did);
	if($r==1)
	 {
	redirect('admin/document', 'refresh');
	 }
		
	}
}