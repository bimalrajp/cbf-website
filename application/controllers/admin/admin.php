<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Home_Controller {
	
	function __construct()
 	{
   		parent::__construct();
	}

	public function index()
	{
		$data['profile'] = $this->admin_model->get_profile();
		$data['users'] = $this->admin_model->get_users();
		$this->load->view('admin/home' ,$data);
	}

	public function pages($page='home',$a='web',$id='')
	{
		if($a=='web')
		{
			if(isset($_SESSION['PKUserID'])&&$_SESSION['PKUserID']!="")
			{
				if(!file_exists(APPPATH.'/views/admin/'.$page.'.php'))
				{
					show_404();
				}
				$data['profile'] = $this->admin_model->get_profile();
				$data['users'] = $this->admin_model->get_users();
				$data['products'] = $this->admin_model->get_products();
				$data['company'] = $this->admin_model->get_company();
				$data['news'] = $this->admin_model->get_news();
				$data['single_news'] = $this->admin_model->get_single_news($id);
				$data['name'] = $this->admin_model->get_name();
				$this->load->view('admin/'.$page,$data);
			}
			else
			{
				redirect(base_url(), 'refresh');
			}
		}
		if($a=='app')
		{
			if($page=='get')
			{
				$res=$this->admin_model->get_users_list();
				echo json_encode($res);
			}
		}
	}

	public function update_profile($id='')
	{
		$res=$this->admin_model->update_profile($id);
		if($res==1)
		{
			$data['profile'] = $this->admin_model->get_profile();
			$this->load->view('admin/profile' ,$data);
		}
	}

	public function update_user($id='')
	{
		$data['profile'] = $this->admin_model->get_profile_user($id);
		$data['products'] = $this->admin_model->get_products();
		$data['company'] = $this->admin_model->get_company();
		$this->load->view('admin/profile_edit' ,$data);	
	}

	public function update_user_profile($id='')
	{
		$res=$this->admin_model->update_user_profile($id);
		if($res==1)
		{
			$data['profile'] = $this->admin_model->get_profile_user($id);
			$this->load->view('admin/profile' ,$data);
		}
	}

	public function profile_view($id='')
	{
		$data['profile'] = $this->admin_model->profile_view($id);
		$this->load->view('admin/profile' ,$data);
	}

	public function get_users()
	{
		$res=$this->admin_model->get_users();
		if($res==1)
		{
			$data['users'] = $this->admin_model->get_users();
			$this->load->view('admin/home' ,$data);
		}
	}

	public function news_create()
	{
		$res=$this->admin_model->set_news();
		if($res==1)
		{
			$data['news'] = $this->admin_model->get_news();
			$this->load->view('admin/news' ,$data);
		}
	}

	public function news_edit($id)
	{
		$res=$this->admin_model->news_edit($id);
		if($res==1)
		{
			$data['news'] = $this->admin_model->get_news();
			$this->load->view('admin/news' ,$data);
		}
	}

	public function news_delete($id)
	{
		$res=$this->admin_model->news_delete($id);
		if($res==1)
		{
			$data['news'] = $this->admin_model->get_news();
			$this->load->view('admin/news' ,$data);
		}
	}

	public function approve_user($id,$a='web')
	{
		if($a=='web')
		{
			$res=$this->admin_model->approve_user($id);
			if($res==1)
			{
				redirect(base_url()."admin/approve_user", 'refresh');
			}
		}
		if($a=='app')
		{
			$res=$this->admin_model->approve_user($id,$a);
			echo json_encode($res);
		}
	}

	public function send_msg()
	{
		$res=$this->admin_model->send_msg();
		redirect(base_url()."admin/message", 'refresh');
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */