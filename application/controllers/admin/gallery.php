<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {



	function __construct()
 	{
   		parent::__construct();
		/*if (!$this->session->userdata('logged_in'))
        { 
            redirect('admin', 'refresh');
        }*/
		$this->load->helper(array('form', 'url'));
		$this->load->model('gallery_model');
 	}
	
	
	
	public function index($a='gallery',$c='')
	{
		$this->load->library('form_validation');
		$session_data=$this->session->userdata('logged_in');
	    $data['username']=$session_data['username'];
		$data['error']=$c;
	    if($a=='gallery')
		{
			$data['gallery']=$this->gallery_model->get_gallery();
			$this->load->view('admin/gallery', $data);
		}
		else if($a=='addgallery')
		{
			$this->load->view('admin/addgallery', $data);
		}
		else if($a=='addgalleryimg')
		{
			$data['galleryid']=$c;
			$this->load->view('admin/addgalleryimg', $data);
		}
		else if($a=='galleryview')
		{
			$gname=$this->gallery_model->get_gallerybyid($c);
			$data['gid'] =$c;
			$data['gallery'] = $this->gallery_model->get_galleryimgsbyid($c);
			$this->load->view('admin/galleryview', $data);
		}
	}
	
	
	
	public function create($a="",$id="")
	{
	 /* $this->gallery_model->set_gallery();
	  $r=$data['gallery']=$this->gallery_model->get_gallery();
	  $this->load->view('admin/gallery', $data);*/
	  
	  if($a=='addgallery')
	  	{
			$r=$this->gallery_model->set_gallery();
			if($r==1)
			{
		  		redirect(base_url().'admin/gallery', 'refresh');
		 	}
		}
	 if($a=='addgalleryimg')
		{
			$r=$this->gallery_model->set_galleryimg($id);
			if($r==1)
			{
				redirect('admin/gallery/galleryview/'.$id, 'refresh');
			}
		}
	  
	  
	  
	}
	

public function delete($gid='',$iv_id='')
	{
		$path='./assets/upload_galleryimg/';
		$pathgi='';
		$gname=$this->gallery_model->get_gallerybyid($gid);
		foreach ($gname as $gn):
		$pathgi='./assets/upload_gallery/'.$gn['Logo'];
		endforeach;
		if($iv_id!='')
		{
			$iname=$this->gallery_model->get_galleryimgbyid($iv_id);
			foreach ($iname as $in):
			$path=$path.$in['Image'];
			endforeach;
			$this->gallery_model->dlt_galleryimgs($path,$iv_id);
			redirect('admin/gallery/galleryview/'.$gid, 'refresh');
			//$this->index('galleryview',$gid);
		}
		else
		{
			$this->gallery_model->dlt_gallery($path,$pathgi,$gid);
			redirect('admin/gallery', 'refresh');
		}
		
	}

public function edit($gid='')
	{
	$data['gallery']=$this->gallery_model->get_gallerybyid($gid);
	$this->load->view('admin/editgallery',$data);
	
	}
	
public function editgallery($gid='')
	{
	$r=$this->gallery_model->edit_gallery($gid);
	if($r==1)
	 {
	redirect('admin/gallery', 'refresh');
	 }
		
	}
}