<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['verifylogin/(:any)'] = "home/verifylogin/$1";
$route['logout/([a-z]+)'] = "home/logout/$1";
$route['search'] = "home/search";
$route['user/register/(:any)'] = "home/register/$1";

$route['user/update_profile/(:any)'] = "user/user/update_profile/$1";
$route['user/passwordchange/(:any)'] = "user/user/update_password/$1";
// App APIs Starts //
$route['user/chatmsg/([a-z]+)/([a-z]+)'] = "user/chat/msg/$1/$2";
$route['user/chatmsg/([a-z]+)/([a-z]+)/(:num)'] = "user/chat/msg/$1/$2/$3";

$route['admin/users/([a-z]+)/([a-z]+)'] = "admin/admin/pages/$1/$2";
$route['admin/users/approve/([a-z]+)/([a-z]+)'] = "admin/admin/approve_user/$1/$2";
$route['search/([a-z]+)'] = "home/search/$1";
// App APIs Ends //

$route['user/([a-z]+)/([a-z]+)'] = "user/user/pages/$1/$2";
$route['user/(:any)'] = "user/user/pages/$1";
$route['user'] = "user/user";

$route['admin/send_msg'] = "admin/admin/send_msg";

$route['admin/news/create']="admin/admin/news_create";
$route['admin/news/update_news/(:num)']="admin/admin/news_edit/$1";
$route['admin/news/delete/(:num)']="admin/admin/news_delete/$1";

$route['admin/document/create/(:any)']="admin/document/create/$1";
$route['admin/document/create/(:a-z)/(:num)']="admin/document/create/$1/$2";
$route['admin/document/delete/(:num)']="admin/document/delete/$1";
/*$route['admin/document/edit/(:num)']="admin/document/edit/$1";*/
$route['admin/document/editdocument/(:num)']="admin/document/editdocument/$1";
$route['admin/document/documentview/delete/(:num)/(:num)']="admin/document/delete/$1/$2";
$route['admin/document/(:a-z)/(:num)']="admin/document/index/$1/$2";
$route['admin/document/(:any)']="admin/document/index/$1";
$route['admin/document']="admin/document";

$route['admin/gallery/create/(:any)']="admin/gallery/create/$1";
$route['admin/gallery/create/(:a-z)/(:num)']="admin/gallery/create/$1/$2";
$route['admin/gallery/delete/(:num)']="admin/gallery/delete/$1";
$route['admin/gallery/edit/(:num)']="admin/gallery/edit/$1";
$route['admin/gallery/editgallery/(:num)']="admin/gallery/editgallery/$1";
$route['admin/gallery/galleryview/delete/(:num)/(:num)']="admin/gallery/delete/$1/$2";
$route['admin/gallery/(:a-z)/(:num)']="admin/gallery/index/$1/$2";
$route['admin/gallery/(:any)']="admin/gallery/index/$1";
$route['admin/gallery']="admin/gallery";

$route['admin/update_user/(:num)'] = "admin/admin/update_user/$1";
$route['admin/update_user_profile/(:num)'] = "admin/admin/update_user_profile/$1";
$route['admin/profile_view/(:num)'] = "admin/admin/profile_view/$1";
$route['admin/update_profile'] = "admin/admin/update_profile";
$route['admin/approve_user/(:num)'] = "admin/admin/approve_user/$1";

$route['admin/(:any)/([a-z]+)/(:num)'] = "admin/admin/pages/$1/$2/$3";
$route['admin/(:any)/([a-z]+)'] = "admin/admin/pages/$1/$2";
$route['admin/(:any)'] = "admin/admin/pages/$1";
$route['admin'] = "admin/admin";

$route['(:any)'] = 'home/view/$1';
$route['default_controller'] = "home";
$route['404_override'] = '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */