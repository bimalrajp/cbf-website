<?php require_once("header.php"); ?>
		<script type="text/javascript">
			function social(a)
			{
				if(a==0)
				{
					alert(a);
				}
				else if(a==1)
				{
					alert(a);
				}
			}
			function isNumberKey(evt)
			{
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
				return true;
			}
		</script>

		<div class="content" style="padding-top:50px;">
			
			
			
            <!-----profile edit starts----->
			<div class="compose">
           
             <?php foreach ($profile as $item): ?>
           
				<div class="forms">
                 	<?php
					if($item['PKUserID']==1)
					 {
				 	 ?>
                  <form action="<?php echo base_url(); ?>admin/update_profile" method="post" enctype="multipart/form-data">
                    <?php
					  }
					 else
					 {
				   	?>
                   <form action="<?php echo base_url(); ?>admin/update_user_profile/<?php echo $item['PKUserID'] ?>" method="post" enctype="multipart/form-data">
                    <?php
					 }
					?>
					<div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-time">Full Name</label>
                        <?php
						if($item['UserFullName']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-time" type="text" value="<?php echo $item['UserFullName'] ?>" style="color:#fff;" name="UserFullName">                        <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-time" type="text" name="UserFullName">
                        <?php
						 }
						 ?>
					</div>
					<div class="group clearfix slideInLeft animated">
						<label class="pull-left" for="compose-date">Gender</label>
						<select class="pull-right" name="UserGender" id="UserGender">
							<option  <?php if($item['UserGender']=='Male'){echo "selected";} ?> value="Male">Male</option>
							<option <?php if($item['UserGender']=='Female'){echo "selected";} ?> value="Female">Female</option>
						</select>
					</div>
					<div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">House Name</label>                        <?php
						if($item['UserHouseName']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['UserHouseName'] ?>" name="UserHouseName">
                         <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserHouseName">
                        <?php
						 }
						 ?>
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Address</label>
                         <?php
						if($item['UserAddress']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['UserAddress'] ?>"  name="UserAddress">
                         <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserAddress">
                        <?php
						 }
						 ?>
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">City</label>
                        <?php
						if($item['UserCity']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['UserCity'] ?>" name="UserCity">
                         <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserCity">
                        <?php
						 }
						 ?>
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Pin</label>
                         <?php
						if($item['UserPin']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['UserPin'] ?>" name="UserPin" onKeyPress="return isNumberKey(event)" maxlength="6" minlength="6">
                        <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserPin" onKeyPress="return isNumberKey(event)" maxlength="6" minlength="6">
                        <?php
						 }
						 ?>
                        
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Land Line</label>
                        <?php
						if($item['UserLandLine']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['UserLandLine'] ?>" name="UserLandLine" onKeyPress="return isNumberKey(event)" maxlength="11" minlength="10">
                        <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserLandLine" onKeyPress="return isNumberKey(event)" maxlength="11" minlength="10">
                        <?php
						 }
						 ?>
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Mobile</label>
                         <?php
						if($item['UserMobile']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['UserMobile'] ?>" name="UserMobile" onKeyPress="return isNumberKey(event)" maxlength="11" minlength="10">
                         <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserMobile" onKeyPress="return isNumberKey(event)" maxlength="11" minlength="10">
                        <?php
						 }
						 ?>
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Email</label>
                        <?php
						if($item['UserEmail']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['UserEmail'] ?>" name="UserEmail">
                        <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserEmail">
                        <?php
						 }
						 ?>
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Company Name</label>
                        <?php
						if($item['UserCompanyName']!="")
					 	{
				 	    ?>
						<select class="pull-right" name="UserCompanyName" id="UserCompanyName">
							<?php if($item['UserCompanyName'] == 0){?><option selected="selected" value="0">Select Company</option><?php }?>
							<?php foreach ($company as $company1): ?>
							<option <?php if($item['UserCompanyName'] == $company1['PKCompanyID']){echo "selected='selected'";} ?> value="<?php echo $company1['PKCompanyID'];?>"><?php echo ucwords(strtolower($company1['CompanyName'])) ?></option>
							<?php endforeach ?>
						</select>
						<!--<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['CompanyName'] ?>" name="UserCompanyName">-->
                         <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserCompanyName">
                        <?php
						 }
						 ?>
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Service</label>
                        <?php
						if($item['UserService']!="")
					 	{
				 	    ?>
						<select class="pull-right" name="UserService" id="UserService">
							<?php foreach ($products as $product): ?>
							<option <?php if($item['UserService'] == $product['PKProductID']){echo "selected='selected'";} ?> value="<?php echo $product['PKProductID'];?>"><?php echo ucwords(strtolower($product['ProductName'])) ?></option>
							<?php endforeach ?>
						</select>
						<!--<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['ProductName'] ?>" name="UserService">-->
                         <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserService">
                        <?php
						 }
						 ?>
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Username</label>
                        <?php
						if($item['UserName']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-title" type="text" value="<?php echo $item['UserName'] ?>" name="UserName">
                         <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="UserName">
                        <?php
						 }
						 ?>
					</div>
                    <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Password</label>
                        <?php
						if($item['Password']!="")
					 	{
				 	    ?>
						<input class="pull-right" id="compose-title" type="text" value="<?php echo $this->encrypt->decode($item['Password']) ?>" name="Password">
                         <?php
					     }
					   else
						 {
				   		?>
                        <input class="pull-right" id="compose-title" type="text" name="Password">
                        <?php
						 }
						 ?>
					</div>
                     <div class="group clearfix slideInRight animated">
						<label class="pull-left" for="compose-title">Image</label>
                         <?php
						if($item['UserImage']!="")
					 	{
				 	    ?>
						<!--<img src="<?php echo base_url(); ?>assets/user/<?php echo $item['UserImage'] ?>" class="pull-right" id="compose-title" >-->
                        <div style=" background-image:url(<?php echo base_url(); ?>assets/user/<?php echo $item['UserImage'] ?>); background-size:cover; width:150px; height:150px;"  class="pull-right" id="compose-title">
						<input type="hidden" name="photo1" value="<?php echo $item['UserImage'] ?>"/>
						<input type="file" name="photo" style="width:150px; height:150px; opacity:0"/>
						</div>
                         <?php
					     }
					   else
						 {
				   		?>
                       <!-- <input class="pull-right" id="compose-title" type="file" name="photo">-->
                        <div style=" background-image:url(<?php echo base_url(); ?>assets/img/profile.png); background-size:cover; width:150px; height:150px;"  class="pull-right" id="compose-title">
						<input type="hidden" name="photo1" value="<?php echo $item['UserImage'] ?>"/>
						<input type="file" name="photo" style="width:150px; height:150px; opacity:0"/>
						</div>
                        <?php
						 }
						 ?>
					</div>
                   <div class="group clearfix slideInLeft animated">
						<label class="visible" for="compose-detail">Description</label>
                         <?php
						if($item['UserDescription']!="")
					 	{
				 	    ?>
						<textarea class="visible" id="compose-detail" rows="3"  name="UserDescription"><?php echo $item['UserDescription'] ?></textarea>
                         <?php
					     }
					   else
						 {
				   		?>
                        <textarea class="visible" id="compose-detail" rows="3" name="UserDescription"></textarea>
                        <?php
						 }
						 ?>
					</div>
                   
                  <div class="action flipInY animated">
						<button class="btn">Update</button>
					</div>
				</div>
                </form>
             <?php endforeach ?>
			</div>
            <!-----profile edit starts----->
           
		
	</div>
</section>
		</div>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>

        <script src="<?php echo base_url(); ?>assets/js/index1.js"></script>

    
    
    
  </body>
</html>