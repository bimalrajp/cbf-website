<?php require_once("header.php"); ?>
	<script type='text/javascript'>
    function getProduct(a)
    {
        var xhr;
        if(window.XMLHttpRequest)
        {
            xhr	= new XMLHttpRequest();
        }
        else
        {
            xhr	= new ActiveXObject('Microsoft.XMLHTTP');
        }
        xhr.onreadystatechange=function()
            {
            if(xhr.readyState==2)
            {
                document.getElementById('company').innerHTML='Loading...';
            }
            else if(xhr.readyState==4 && xhr.status==200)
            {
                var res	=	xhr.responseText;
                document.getElementById('company').innerHTML=''+res;
            }
        }
        var product=a;
        xhr.open('GET','<?php echo base_url(); ?>ajax/ajax_file_companies.php?product='+product);
        xhr.send();
    }
        
    function getCompany(b)
    {
        var xhr;
        if(window.XMLHttpRequest)
        {
            xhr	= new XMLHttpRequest();
        }
        else
        {
            xhr	= new ActiveXObject('Microsoft.XMLHTTP');
        }
        xhr.onreadystatechange=function()
        {
            if(xhr.readyState==2)
            {
                document.getElementById('users').innerHTML='Loading...';
            }
            else if(xhr.readyState==4 && xhr.status==200)
            {
                var res	=	xhr.responseText;
                document.getElementById('users').innerHTML=''+res;
            }
        }
        var company=b;
        xhr.open('GET','<?php echo base_url(); ?>ajax/ajax_file_names.php?company='+company);
        xhr.send();
    }
    </script>
				<div class="content">
					<!-----profile edit starts----->
					<div class="compose" style="padding-top:50px;">
						<div class="forms">
							<form action="<?php echo base_url();?>admin/send_msg" method="post" id="frm_message" name="frm_message">
								<div class="group clearfix slideInLeft animated">
									<label class="pull-left" for="compose-date">Product/Services</label>
									<select class="pull-right" name="product" id="product"  onChange="getProduct(this.value)">
										<option value="0">Select Product</option>
										<?php foreach ($products as $item): ?>
										<option value="<?php echo $item['PKProductID'] ?>"><?php echo $item['ProductName'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="group clearfix slideInLeft animated">
									<label class="pull-left" for="compose-date">Company Name</label>
									<select class="pull-right" name="company" id="company"  onChange="getCompany(this.value)"  >
										<option value="0">Select Company</option>
									</select>
								</div>
								<div class="group clearfix slideInLeft animated">
									<label class="pull-left" for="compose-date">Name</label>
									<select class="pull-right" name="users" id="users">
										<option value="0">Select name</option>
									</select>
								</div>
								<div class="group clearfix slideInLeft animated">
									<label class="visible" for="compose-detail">Type Your Messge here</label>
									<textarea class="visible" id="compose-detail" rows="3"  name="UserMessage"></textarea>
								</div>
								<div class="action flipInY animated">
									<button class="btn">Send</button>
								</div>
							</div>
						</form>
					</div>
					<!-----profile edit starts----->
				</div>
			</section>
		</div>
		<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>
		<script src="<?php echo base_url(); ?>assets/js/index1.js"></script>
	</body>
</html>