<?php require_once("header.php"); ?>

		 
         <div class="content" style="padding-top:50px;">
			
			 <!-----Approve users starts----->
            
			<div class="chats">
				<div class="tabs-list clearfix">
					<a href="#" class="tab active">Users</a>
					
				</div>
				<div class="active-users">
                 <?php foreach ($users as $item): ?>
					<div class="user clearfix rotateInDownLeft animated">
						<div class="photo pull-left">
				 <?php
					if($item['UserImage']=="")
					 {
				  ?>
					<img src="<?php echo base_url(); ?>assets/img/team-member.jpg">
                  <?php
					 }
					else
					 {
				   ?>
                     <img src="<?php echo base_url(); ?>assets/user/<?php echo $item['UserImage'] ?>">
                   <?php
					 }
				   ?>
						</div>
						<div class="desc pull-left">
							<a href="<?php echo base_url(); ?>admin/profile_view/<?php echo $item['PKUserID'] ?>" style="text-decoration:none;"><p class="name"><?php echo $item['UserFullName'] ?></p></a>
							<p class="position"><?php echo $item['CompanyName'] ?></p>
						</div>
                        
                         
						<!--<div class="idle pull-right"><span class="away"></span></div>-->
                        <form action="<?php echo base_url(); ?>admin/approve_user/<?php echo $item['PKUserID'] ?>" method="post">
                        
                         <?php
						  //echo $item['UserFlag'];
						  if($item['UserFlag']=="1")
					 	  {
				  		 ?>
                        
                        <div class="action pull-right"  style="visibility:hidden">
                        <input id="gear-<?php echo $item['PKUserID'] ?>" class="on-off" type="checkbox" value="1" name="UserFlag" onchange="this.form.submit()" checked="checked">
                        <label for="gear-<?php echo $item['PKUserID'] ?>"></label>
                        </div>
                        
                        <div class="onoffswitch pull-right">
    					<input type="checkbox" class="onoffswitch-checkbox" id="myonoffswitch<?php echo $item['PKUserID'] ?>" value="1" name="onoffswitch" checked="checked" onchange="this.form.submit()">
   					    <label class="onoffswitch-label" for="myonoffswitch<?php echo $item['PKUserID'] ?>">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
    </label>
</div>
                        
                        <?php
					     }
					    else if($item['UserFlag']=="0")
					     {
				        ?>
                         <div class="action pull-right"  style="visibility:hidden">
                        <input id="gear-<?php echo $item['PKUserID'] ?>" class="on-off" type="checkbox" value="0" name="UserFlag" onchange="this.form.submit()">
                        <label for="gear-<?php echo $item['PKUserID'] ?>"></label>
                        </div>
                                 <div class="onoffswitch pull-right">
    <input type="checkbox"  class="onoffswitch-checkbox" id="myonoffswitch<?php echo $item['PKUserID'] ?>" name="onoffswitch"  onchange="this.form.submit()" value="1">
    <label class="onoffswitch-label" for="myonoffswitch<?php echo $item['PKUserID'] ?>">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>
       
                        <?php
					     }
						 ?>
                        </form>
					
					</div>
                  <?php endforeach ?>   
             
			</div>
              
            <!-----Approve users starts----->
			<div class="html settings">
				<div class="setting-list">
					<div class="gear clearfix slideInRight animated">
						<div class="title pull-left">General</div>
						<div class="action pull-right"><span class="ion-ios-arrow-right"></span></div>
					</div>
					<div class="gear clearfix slideInLeft animated">
						<div class="title pull-left"><label for="gear-notice">Notification</label></div>
						<div class="action pull-right"><input id="gear-notice" class="on-off" type="checkbox"><label for="gear-notice"></label></div>
					</div>
					<div class="gear clearfix slideInRight animated">
						<div class="title pull-left"><label for="gear-sound">Sound</label></div>
						<div class="action pull-right"><input id="gear-sound" class="on-off" type="checkbox" checked><label for="gear-sound"></label></div>
					</div>
					<div class="gear clearfix slideInLeft animated">
						<div class="title pull-left">Theme</div>
						<div class="action pull-right">Standard <span class="ion-ios-arrow-right"></span></div>
					</div>
					<div class="gear clearfix slideInRight animated">
						<div class="title pull-left">Support</div>
						<div class="action pull-right"><span class="ion-ios-arrow-right"></span></div>
					</div>
					<div class="gear clearfix slideInLeft animated">
						<div class="title pull-left">Privacy</div>
						<div class="action pull-right"><span class="ion-ios-arrow-right"></span></div>
					</div>
				</div>
			</div>
            
            <!-----profile view starts----->
             <?php foreach ($profile as $item): ?>
			<div class="html profile">
				<div class="photo flipInX animated">
                  <?php
					if($item['UserImage']=="")
					 {
				  ?>
					<img src="<?php echo base_url(); ?>assets/img/team-member.jpg">
                  <?php
					 }
					else
					 {
				   ?>
                     <img src="<?php echo base_url(); ?>assets/user/<?php echo $item['UserImage'] ?>">
                   <?php
					 }
				   ?>
                  
					<div class="social">
						<a href="http://facebook.com/khadkamhn" class="soc-item soc-count-1"><span class="ion-social-facebook"></span></a>
						<a href="http://twitter.com/khadkamhn" class="soc-item soc-count-2"><span class="ion-social-twitter"></span></a>
						<a href="http://github.com/khadkamhn" class="soc-item soc-count-3"><span class="ion-social-github"></span></a>
						<a href="http://pinterest.com/khadkamhn" class="soc-item soc-count-4"><span class="ion-social-pinterest"></span></a>
						<a href="http://np.linkedin.com/in/khadkamhn" class="soc-item soc-count-5"><span class="ion-social-linkedin"></span></a>
						<a href="http://codepen.io/khadkamhn" class="soc-item soc-count-6"><span class="ion-social-codepen"></span></a>
						<a href="skype:khadkamhn?userinfo" class="soc-item soc-count-7"><span class="ion-social-skype"></span></a>
						<a href="http://dribbble.com/khadkamhn" class="soc-item soc-count-8"><span class="ion-social-dribbble"></span></a>
					</div>
				</div>
				<div class="details">
					<p class="heading flipInY animated">
						<span class="name"><?php echo $item['UserFullName'] ?></span><span class="position">Web/Graphic Desiger</span>
					</p>
					<p class="text fadeInUp animated">
                    <ul style="list-style: circle; padding-left:400px; padding-top:50px; line-height:40px; font-family: 'Montserrat', sans-serif; font-size:20px;">
                     <?php
					  if($item['UserGender']!="")
					  {
				     ?>
                    <li><b>Gender : </b><?php echo $item['UserGender'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserHouseName']!="")
					  {
				     ?>
                    <li><b>House Name : </b><?php echo $item['UserHouseName'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserAddress']!="")
					  {
				     ?>
                    <li><b>Address :</b> <?php echo $item['UserAddress'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserCity']!="")
					  {
				     ?>
                    <li><b>City :</b> <?php echo $item['UserCity'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserPin']!="")
					  {
				     ?>
                    <li><b>Pin :</b> <?php echo $item['UserPin'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserLandLine']!="")
					  {
				     ?>
                    <li><b>LandLine :</b> <?php echo $item['UserLandLine'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserMobile']!="")
					  {
				     ?>
                    <li><b>Mobile :</b> <?php echo $item['UserMobile'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserEmail']!="")
					  {
				     ?>
                    <li><b>Email :</b> <?php echo $item['UserEmail'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserCompanyName']!="")
					  {
				     ?>
                    <li><b>Company Name :</b> <?php echo $item['UserCompanyName'] ?></li>
                     <?php
					  }
					 ?>
                     </ul>
                    </p>
				</div>
			</div>
             <?php endforeach ?>
           <!-----profile view ends----->
           
			<div class="html credits">
				<div class="title flipInY animated">I have been using the following assets to build this design</div>
				<div class="credit-ol">
					<div class="credit-li lightSpeedIn animated"><a href="https://www.google.com/fonts/specimen/Roboto" target="_blank">roboto</a> <span>for typography</span></div>
					<div class="credit-li lightSpeedIn animated"><a href="https://jquery.com" target="_blank">jquery</a> <span>for design/ui</span></div>
					<div class="credit-li lightSpeedIn animated"><a href="http://ionicons.com/" target="_blank">ionicons</a> <span>for icons</span></div>
					<div class="credit-li lightSpeedIn animated"><a href="http://uifaces.com/authorized" target="_blank">ui faces</a> <span>for avatar</span></div>
					<div class="credit-li lightSpeedIn animated"><a href="https://daneden.github.io/animate.css/" target="_blank">animate.css</a> <span>for animation</span></div>
					<div class="credit-li lightSpeedIn animated"><a href="https://dribbble.com/shots/1928064-Secret-Project" target="_blank">concept of design</a> <span>for layout</span></div>
					<div class="credit-li lightSpeedIn animated"><a href="https://unsplash.com/photos/6asyCyR0K1Q/download" target="_blank">unsplash.com</a> <span>for background</span></div>
				</div>
				<div class="text zoomInUp animated">I'm glad for using these resources and expecting same as time ahead</div>
			</div>
		</div>
			
            <!-----Approve users ends----->
            
            
		
	</div>
</div>
   </section>
		</div>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>

        <script src="<?php echo base_url(); ?>assets/js/index1.js"></script>

    
    
    
  </body>
</html>
