<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Catholic Business Forum</title>
    
      <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/logo.png">
     
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css1/responsive.css">

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
      
      
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css1/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Serif' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/reset.css">
    <!-- CSS reset -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/reset1.css">
    <!-- Gem style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/product.css">
    <!-- Gem style -->
    <script src="<?php echo base_url(); ?>assets/js1/modernizr.js"></script>
    <!-- Modernizr -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/animate.min.css">

    <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>
    <script src="<?php echo base_url(); ?>assets/js1/index.js"></script>
    <script src="<?php echo base_url(); ?>assets/js1/carousel.js"></script>

    <!--scrolling animation-->
    <script src="<?php echo base_url(); ?>assets/js1/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>

   <!------marquee style starts----------------->
    <style>

body { margin: 20px; }

.marquee {
  height: 25px;
  width: 100%;

  overflow: hidden;
  position: relative;
}

.marquee div {
  display: block;
  width: 200%;
  height: 30px;
  background:#b8b8b8;
  color:#373737;
  padding:5px;

  position: absolute;
  overflow: hidden;

  animation: marquee 30s linear infinite;
}

.marquee span {
  float: left;
  width: 50%;
}

@keyframes marquee {
  0% { left: 0; }
  100% { left: -100%; }
}
</style>
   <!------marquee style ends----------------->

</head>
<body>
    <div>

        <header style="background:#4b6c8f">
			<div class="wrapper">
				<a href="#"><img src="<?php echo base_url(); ?>assets/img/logo2.png" class="logo" alt="" titl=""/></a>
				<a href="#" class="hamburger"></a>
				<nav>
					<ul>
						<li><a href="<?php echo base_url(); ?>" style="text-decoration:none">Home</a></li>
						<li><a href="<?php echo base_url(); ?>profile" style="text-decoration:none">Profile</a></li>
                        <li><a href="<?php echo base_url(); ?>GoverningBody" style="text-decoration:none">Governing Body</a></li>
						<li><a href="<?php echo base_url(); ?>objectives" style="text-decoration:none">Objectives</a></li>
						<li><a href="<?php echo base_url(); ?>gallery" style="text-decoration:none">Gallery</a></li>
						<li><a href="<?php echo base_url(); ?>directory" style="text-decoration:none">Directory</a></li>
                        <li><a href="<?php echo base_url(); ?>documents" style="text-decoration:none">Document</a></li>
						<li><a href="<?php echo base_url(); ?>contact" style="text-decoration:none">Contact Us</a></li>
					</ul>
					<a href="<?php echo base_url(); ?>register" class="login_btn">Login/Register</a>
				</nav>
			</div>
		</header>

        <div>
            <a id="Home"></a>
            <div class="bs-example">
                <div id="myCarousel" class="carousel slide" data-interval="2000" data-ride="carousel">
                    <!-- Carousel indicators -->
                    <ol class="carousel-indicators">
                        <li class="slide-one active"></li>
                        <li class="slide-two"></li>
                        <li class="slide-three"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item" style="background-image: url('<?php echo base_url(); ?>assets/images/slide1.jpg')">
                            <div class="wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="2s" style="visibility: visible; -webkit-animation-duration: 2s; -webkit-animation-delay: 300ms; -webkit-animation-iteration-count: infinite;">
                                <!--<h2>Slide 1</h2>-->
                            </div>
                            <!--<div class="carousel-caption">
                                <div class="wow slideInRight">
                                    <h3>First slide label</h3>
                                </div>
                                <div class="wow slideInLeft">
                                    <p>We help to make your business better. </p>
                                </div>
                            </div>-->
                        </div>
                        <div class="item" style="background-image: url('<?php echo base_url(); ?>assets/images/slide2.jpg')">
                            <div class="wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="2s" style="visibility: visible; -webkit-animation-duration: 2s; -webkit-animation-delay: 300ms; -webkit-animation-iteration-count: infinite;">
                               <!-- <h2>Slide 2</h2>-->
                            </div>
                            <!--<div class="carousel-caption">
                                <div class="wow slideInRight">
                                    <h3>Second slide label</h3>
                                </div>
                                <div class="wow slideInLeft">
                                    <p>We build brands</p>
                                </div>
                            </div>-->
                        </div>
                        <div class="item" style="background-image: url('<?php echo base_url(); ?>assets/images/slide3.jpg')">
                            <div class="wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="2s" style="visibility: visible; -webkit-animation-duration: 2s; -webkit-animation-delay: 300ms; -webkit-animation-iteration-count: infinite;">
                                <!--<h2>Slide 3</h2>-->
                            </div>
                            <!--<div class="carousel-caption">
                                <div class="wow slideInRight">
                                    <h3>Third slide label</h3>
                                </div>
                                <div class="wow slideInLeft">
                                    <p>We help to make your business better. </p>
                                </div>
                            </div>-->
                        </div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="carousel-control right">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
            <div class="marquee">
  <div>
   <?php foreach ($news as $row):?>
    <span>&nbsp;&nbsp;&nbsp; <?php echo $row['News'];?></span>
   <?php endforeach; ?>
  </div>
</div>