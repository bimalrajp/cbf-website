<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Catholic Business Forum</title>
      <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/logo.png">
     
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css1/responsive.css">

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
      
      
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css1/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Serif' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/reset.css">
    <!-- CSS reset -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/reset1.css">
    <!-- Gem style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/product.css">
    <!-- Gem style -->
    <script src="<?php echo base_url(); ?>assets/js1/modernizr.js"></script>
    <!-- Modernizr -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/animate.min.css">

    <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>
    <script src="<?php echo base_url(); ?>assets/js1/index.js"></script>
    <script src="<?php echo base_url(); ?>assets/js1/carousel.js"></script>

    <!--scrolling animation-->
    <script src="<?php echo base_url(); ?>assets/js1/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,300,700'>
	<link rel='stylesheet prefetch' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
	<link rel='stylesheet prefetch' href='http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css'>


</head>
<body>
    <div>


<header style="background:#4b6c8f">
			<div class="wrapper">
				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/logo2.png" class="logo" alt="" titl=""/></a>
				<a href="#" class="hamburger"></a>
				<nav>
					<ul>
						<li><a href="<?php echo base_url(); ?>" style="text-decoration:none">Home</a></li>
						<li><a href="<?php echo base_url(); ?>profile" style="text-decoration:none">Profile</a></li>
                         <li><a href="<?php echo base_url(); ?>GoverningBody" style="text-decoration:none">Governing Body</a></li>
						<li><a href="<?php echo base_url(); ?>objectives" style="text-decoration:none">Objectives</a></li>
						<li><a href="<?php echo base_url(); ?>gallery" style="text-decoration:none">Gallery</a></li>
						<li><a href="<?php echo base_url(); ?>directory" style="text-decoration:none">Directory</a></li>
                         <li><a href="<?php echo base_url(); ?>documents" style="text-decoration:none">Documents</a></li>
						<li><a href="<?php echo base_url(); ?>contact" style="text-decoration:none">Contact Us</a></li>
					</ul>
					<a href="<?php echo base_url(); ?>register" class="login_btn">Login/Register</a>
			  </nav>
  </div>
</header>