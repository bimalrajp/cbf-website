<?php require_once("sub_header.php");?>
			<!--<script type='text/javascript'>
				function getDirectoryData(company)
				{
					var xhr;
					if(window.XMLHttpRequest)
					{
						xhr	= new XMLHttpRequest();
					}
					else
					{
						xhr	= new ActiveXObject('Microsoft.XMLHTTP');
					}
					xhr.onreadystatechange=function()
					{
						if(xhr.readyState==2)
						{
							document.getElementById('directorydata').innerHTML='<br/><br/><br/><br/><br/><br/>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<b>Getting the Directory List. Please wait...!</b>';
						}
						else if(xhr.readyState==4 && xhr.status==200)
						{
							var res	=	xhr.responseText;
							document.getElementById('directorydata').innerHTML=''+res;
						}
					}
					xhr.open('GET','<?php echo base_url(); ?>ajax/ajax_file_directory.php?company='+company);
					xhr.send();
				}
			</script>-->
			<!--<style>
				.table
				{
					color:#666;
					font-size:14px;
					font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif;
					text-align:left;
				}
			</style>-->
            
            <div class="row" style="padding-top:5%; padding-left:10px;">
            <div class="col-md-12">
            <div class="panel panel-default"> 
                    <div class="panel-heading">
                            Search
                    </div>  
            <div class="panel-body"> 
												<form method="post" action="<?php echo base_url().'search' ?>">
													<div id="no-more-tables">
														<table class="col-md-12  table-condensed cf">
															<thead class="cf">
																<tr>
                                                                    <th>Name</th>
																	<th>Company Name</th>
																	<th>Product/Service Name</th>
                                                                    <th></th>
																</tr>
															</thead>
															<tbody>
																<tr>
                                                                    <td data-title="Name">
																		<input class="form-control" type="text" name="name" id="company_name"/>
																	</td>
																	<td data-title="Company Name">
																		<input class="form-control" type="text" name="company_name" id="company_name" />
																	</td>
																	<td data-title="Product Name">
																		<input class="form-control" type="text" name="product_name" id="product_name" />
																	</td>
																	<td style="vertical-align:top;">
																		<button type="submit" class="btn btn-primary">Search</button>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</form>
											</div>
            </div>
            </div>
            </div>
            
			<div class="row" style="padding-bottom:10%;">
				<div class="wow bounceInUp">
					<main>
						<!--<a name="WhoWeAre"></a>-->
						<a id="WhoWeAre"></a>
						<section class="cd-section whoweare" style="min-height:512px;">
							<!--<select class="pull-right" name="SelectCompany" id="SelectCompany" onChange="getDirectoryData(this.value);">
								<option value="0">All Companies</option>
								<?php foreach ($companies as $company): ?>
								<option value="<?php echo $company['PKCompanyID'];?>"><?php echo ucwords(strtolower($company['CompanyName'])) ?></option>
								<?php endforeach ?>
							</select>-->
							<div class="cd-container" >
								<h3>Directory</h3>
								<table class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>Full Name</th>
											<th>Company Name</th>
											<th>Mobile</th>
										</tr>
									</thead>
									<tbody id="directorydata">
										<?php $i=1; foreach ($directory as $item): ?>
										<tr>
											<td>
												<?php echo $i++; ?>	
											</td>
											<td>
												<a href="<?php echo base_url(); ?>userdetail/<?php echo $item['PKUserID'];?>" style="text-decoration:none;"><?php echo $item['UserFullName'];?></a>
											</td>
											<td>
												<?php echo $item['CompanyName'];?>
											</td>
											<td>
												<?php echo $item['UserMobile'];?>
											</td>
										</tr>
										<?php endforeach;  ?>
									</tbody>
								</table>
							</div> <!-- cd-container -->
						</section> <!-- cd-team -->
					</main>
				</div>
			</div>
<?php require_once("footer.php");?>