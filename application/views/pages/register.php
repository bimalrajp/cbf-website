<?php require_once("sub_header.php"); ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/reset1.css">
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/style1.css">

<?php
$_SESSION["message"]="";
				if($_SESSION['message']!="")
				{
				?>
				<div style="position:absolute; width:100%; top:25%; z-index:100;" align="center">
					<div id="msg" style="width:60%; display:block; background-color:#0C9; font-family:'Audiowide',cursive; font-size:18px;">
						<div align="center" style="padding:20px; font-family:'Audiowide',cursive;">
							&nbsp;<br/>
							<h1 style="font-variant:small-caps; color:#FFF; cursor:default;"><?php echo $_SESSION['message'];?></h1>
						</div><br/>
						<button class="button button-block" style="cursor:pointer;" onClick="document.getElementById('msg').style.display='none';">Okay</button><br/>&nbsp;
					</div>
				</div>
				<?php
				}
				$_SESSION['message']="";
				?>




<div class="container" align="center" style="padding-top:50px; padding-bottom:20px;">
  <div class="card"></div>
  <div class="card">
    <h1 class="title">Login</h1>
    <form action="<?php echo base_url(); ?>verifylogin/web" method="post">
      <div class="input-container">
        <input type="text" id="Username" name="Username" required/>
        <label for="Username">Username</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="password" id="Password" name="Password" required/>
        <label for="Password">Password</label>
        <div class="bar"></div>
      </div>
      <div class="button-container">
        <button><span>Go</span></button>
      </div>
    <!--  <div class="footer"><a href="#">Forgot your password?</a></div>-->
    </form>
  </div>
  <div class="card alt"  style="background:#86a3cc; padding-bottom:50px">
    <div class="toggle" title="Register"></div>
    <h1 class="title">Register
      <div class="close"></div>
    </h1>
    <form action="<?php echo base_url(); ?>user/register" enctype="multipart/form-data" method="post">
      <div class="input-container">
        <input type="text" id="Name" name="Name" required/>
        <label for="Name">Name</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <select name="Gender" id="Gender">
        <option value="male">Male</option>
        <option value="female">Female</option>
        </select>
        <label for="Gender">Gender</label>
        <div class="bar"></div>
      </div>
     <div class="input-container">
        <input type="text" id="Mobile" name="Mobile" required/>
        <label for="Mobile">Mobile</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="email" id="Email" name="Email" required/>
        <label for="Email">Email</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="text" id="CompanyName" name="CompanyName" required/>
        <label for="CompanyName">Company Name</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="text" id="ProductName" name="ProductName" required/>
        <label for="ProductName">Product Name</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="text" id="UserDescription" name="UserDescription" required/>
        <label for="UserDescription">Description</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="file" id="UserImage" name="UserImage" style="visibility:collapse" onChange="document.getElementById('userimagelabel').innerHTML='Image Selected'" accept="image/*"/>
        <label for="UserImage" id="userimagelabel">Image</label>
        <div class="bar"></div>
      </div>
     <div class="input-container">
        <input type="text" id="RUserName" name="RUserName" required/>
        <label for="UserName">UserName</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="password" id="RPassword" name="RPassword" required/>
        <label for="Password">Password</label>
        <div class="bar"></div>
      </div>
      <div class="button-container">
        <button><span>Submit</span></button>
      </div>
    </form>
    
  </div>
</div>



<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="<?php echo base_url(); ?>assets/js1/index1.js"></script>
<?php /*?><?php require_once("footer.php"); ?><?php */?>