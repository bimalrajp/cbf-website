<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Webkurn technologies</title>
      <link href="<?php echo base_url(); ?>assets/css1/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css1/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Serif' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/reset.css">
    <!-- CSS reset -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/style.css">
    <!-- Gem style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/product.css">
    <!-- Gem style -->
    <script src="<?php echo base_url(); ?>assets/js1/modernizr.js"></script>
    <!-- Modernizr -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css1/animate.min.css">

    <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>
    <script src="<?php echo base_url(); ?>assets/js1/index.js"></script>
    <script src="<?php echo base_url(); ?>assets/js1/carousel.js"></script>

    <!--scrolling animation-->
    <script src="<?php echo base_url(); ?>assets/js1/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>



</head>
<body>
		<?php
		if(isset($_SESSION["message"]))
		{
		if($_SESSION["message"]!="")
		{
		?>
		<div id="msgs" style="position:fixed; width:50%; left:25%; top:25%; z-index:1000;" align="center">
			<div class='panel panel-primary'>
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3 pull-left huge">
							<h1>Message</h1>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<span style="font-size:24px; margin:5px; margin-top:30px; margin-bottom:30px;"><?php echo $_SESSION["message"];?></span>
					<div class="clearfix"></div>
					<button style="margin-top:30px;" class='btn btn-primary' style="cursor:pointer;" onClick="document.getElementById('msgs').style.display='none';">OK</button><br/>&nbsp;
				</div>
			</div>
		</div>
		<?php
		}
		$_SESSION['message']="";
		}
		?>
    <div>

        <nav role="navigation" class="navbar navbar-default navbar-fixed-top" style="background-color:#4b6c8f; margin-bottom: 0px;">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                   <!-- <a href="#" class="navbar-brand" style="font-size: 30px; color: white; font-stretch: wider; font-weight: 600;">--><a href="#"><img src="<?php echo base_url(); ?>assets/img/logo2.png" class="logo" alt="" titl="" style="height:50px;"/></a><!--</a>-->
                </div>

                <div id="navbarCollapse" class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url(); ?>" style="color: white;">Home</a></li>
                        <li><a href="<?php echo base_url(); ?>profile">Profile</a></li>
                        <li><a href="<?php echo base_url(); ?>">Objectives</a></li>
                        <li><a href="<?php echo base_url(); ?>gallary">Gallary</a></li>
                        <li><a href="#Contact_us">Directory</a></li>
                        <li><a href="#Gallary">Feedback</a></li>
                        <li><a href="<?php echo base_url(); ?>contact">Contact Us</a></li>
                        <li><a href="<?php echo base_url(); ?>register" class="login_btn">Login/Register</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div>
            <a id="Home"></a>
            <div class="bs-example">
                <div id="myCarousel" class="carousel slide" data-interval="2000" data-ride="carousel">
                    <!-- Carousel indicators -->
                    <ol class="carousel-indicators">
                        <li class="slide-one active"></li>
                        <li class="slide-two"></li>
                        <li class="slide-three"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item" style="background-image: url('<?php echo base_url(); ?>assets/Images/slide1.jpg')">
                            <div class="wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="2s" style="visibility: visible; -webkit-animation-duration: 2s; -webkit-animation-delay: 300ms; -webkit-animation-iteration-count: infinite;">
                                <h2>Slide 1</h2>
                            </div>
                            <div class="carousel-caption">
                                <div class="wow slideInRight">
                                    <h3>First slide label</h3>
                                </div>
                                <div class="wow slideInLeft">
                                    <p>We help to make your business better. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item" style="background-image: url('<?php echo base_url(); ?>assets/Images/slide2.jpg')">
                            <div class="wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="2s" style="visibility: visible; -webkit-animation-duration: 2s; -webkit-animation-delay: 300ms; -webkit-animation-iteration-count: infinite;">
                                <h2>Slide 2</h2>
                            </div>
                            <div class="carousel-caption">
                                <div class="wow slideInRight">
                                    <h3>Second slide label</h3>
                                </div>
                                <div class="wow slideInLeft">
                                    <p>We build brands</p>
                                </div>
                            </div>
                        </div>
                        <div class="item" style="background-image: url('<?php echo base_url(); ?>assets/Images/slide3.jpg')">
                            <div class="wow pulse animated" data-wow-delay="300ms" data-wow-iteration="infinite" data-wow-duration="2s" style="visibility: visible; -webkit-animation-duration: 2s; -webkit-animation-delay: 300ms; -webkit-animation-iteration-count: infinite;">
                                <h2>Slide 3</h2>
                            </div>
                            <div class="carousel-caption">
                                <div class="wow slideInRight">
                                    <h3>Third slide label</h3>
                                </div>
                                <div class="wow slideInLeft">
                                    <p>We help to make your business better. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="carousel-control right">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
            