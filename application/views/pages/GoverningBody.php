<?php require_once("sub_header.php");?>

<div class="row" style="padding-bottom:10%;">


                <div class="wow bounceInUp">
                    <main>
        <!--<a name="WhoWeAre"></a>-->
          <!--<a id="WhoWeAre"></a>-->
		<section id="cd-team" class="cd-section whoweare">
			<div class="cd-container">
				<h3>Governing Body</h3>
                <div class="gallery_frame_img" style="width:59%; height:80%; border:#000; vertical-align:top; display:inline-block; padding-bottom:5px;">
                <p style="font-size: 36px; text-align: center; padding:5px;">Patrons</p>
                <div>
                
				<div id="clm2">
          <div class="gallery_frame_img" style="width:100%"> 

<img src="<?php echo base_url(); ?>assets/images/george_alenchery.jpg" style="width:90%; height:90%; margin-left:5%; padding:5px;"/>
<h4 style="color:#666">Mar George Cardinal Alenchery
</h4>
        </div>
</div>
				<div id="clm2">
          <div class="gallery_frame_img" style="width:100%"> 

<img src="<?php echo base_url(); ?>assets/images/Mar Sebastian Adayanthrath.jpg" style="width:90%; height:90%; margin-left:5%;padding:5px;"/>
<h4 style="color:#666">Mar Sebastian Adayanthrath
</h4>
        </div>
</div>
				<div id="clm2">
          <div class="gallery_frame_img" style="width:100%"> 

<img src="<?php echo base_url(); ?>assets/images/mar-jose-puthenveettil.jpg" style="width:90%; height:90%; margin-left:5%;padding:5px;"/>
<h4 style="color:#666">Mar Jose Puthenveettil
</h4>
        </div>
</div>

				</div>
                
				</div>
				<div class="gallery_frame_img" style="width:19.5%; height:80%; vertical-align:top; display:inline-block; padding-bottom:5px;">
                <p style="font-size: 36px; text-align: center; padding:5px;">President</p>
                <div>
                
				<div id="clm1">
          <div class="gallery_frame_img1" style="width:100%"> 

<img src="<?php echo base_url(); ?>assets/images/fr_varghese_ponthempilly.jpg" style="width:90%; height:90%; margin-left:5%;padding:5px;"/>
<h4 style="color:#666">Fr. Varghese Ponthempilly
</h4>
        </div>
</div>

				</div>
                </div>
				<div class="gallery_frame_img" style="width:19.5%; height:80%; vertical-align:top; display:inline-block; padding-bottom:5px;">
                <p style="font-size: 36px; text-align: center; padding:5px;">Secretary</p>
                <div>
                
                <div id="clm1">
          <div class="gallery_frame_img1" style="width:100%"> 

<img src="<?php echo base_url(); ?>assets/images/jose_ettuparayil.jpg" style="width:90%; height:90%; margin-left:5%;padding:5px;"/>
<h4 style="color:#666">Mr Jose Ettuparayil
</h4>
        </div>
</div>

				</div>
                </div>
			</div> <!-- cd-container -->
		</section> <!-- cd-team -->

	</main>
                </div>


            </div>

<?php require_once("footer.php");?>