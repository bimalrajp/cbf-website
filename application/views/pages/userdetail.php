<?php require_once("sub_header.php"); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style1.css">

		<div class="content">
			
			
            <!-----profile view starts----->
             <?php foreach ($profile as $item): ?>
			 
			<div class="profile">
				<div class="photo flipInX animated">
               
                  <?php
					if($item['UserImage']=="")
					 {
				  ?>
					<!--<img src="<?php echo base_url(); ?>assets/img/team-member.jpg" >-->
                     <div style="background:url(<?php echo base_url(); ?>assets/img/profile.png); width:100%; height:100%; background-size:cover">
                  <?php
					 }
					else
					 {
				   ?>
                    <!-- <img src="<?php echo base_url(); ?>assets/user/<?php echo $item['UserImage'] ?>">-->
                     <div style="background:url(<?php echo base_url(); ?>assets/user/<?php echo $item['UserImage'] ?>); width:100%; height:100%; background-size:cover">
                   <?php
					 }
				   ?>
                  
                  </div>
                   <?php
				   if($item['PKUserID']!=1)
					{
				   ?>
					<div class="social">
						<!--<a href="http://facebook.com/khadkamhn" class="soc-item soc-count-1"><span class="ion-social-facebook"></span></a>
						<a href="http://twitter.com/khadkamhn" class="soc-item soc-count-2"><span class="ion-social-twitter"></span></a>
						<a href="http://github.com/khadkamhn" class="soc-item soc-count-3"><span class="ion-social-github"></span></a>
						<a href="http://pinterest.com/khadkamhn" class="soc-item soc-count-4"><span class="ion-social-pinterest"></span></a>
						<a href="http://np.linkedin.com/in/khadkamhn" class="soc-item soc-count-5"><span class="ion-social-linkedin"></span></a>
						<a href="http://codepen.io/khadkamhn" class="soc-item soc-count-6"><span class="ion-social-codepen"></span></a>
						<a href="skype:khadkamhn?userinfo" class="soc-item soc-count-7"><span class="ion-social-skype"></span></a>
						<a href="http://dribbble.com/khadkamhn" class="soc-item soc-count-8"><span class="ion-social-dribbble"></span></a>-->
                   </div>
                    <?php
					 }
				   ?>
                   
                   
				</div>
                
				<div class="details" style="color:#FFF;">
					<p class="heading flipInY animated">
						<span class="name"><?php echo $item['UserFullName'] ?></span><span class="position"><?php if($item['UserCompanyName']!=""){ echo $item['CompanyName']; }?></span>
					</p>
					<p class="text fadeInUp animated">
                    <ul style="list-style: circle; padding-top:50px; line-height:40px; font-family: 'Montserrat', sans-serif; font-size:20px;" align="center">
                     <?php
					  if($item['UserGender']!="")
					  {
				     ?>
                    <li><b>Gender :</b> <?php echo $item['UserGender'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserHouseName']!="")
					  {
				     ?>
                    <li><b>House Name : </b><?php echo $item['UserHouseName'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserAddress']!="")
					  {
				     ?>
                    <li><b>Address :</b> <?php echo $item['UserAddress'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserCity']!="")
					  {
				     ?>
                    <li><b>City :</b> <?php echo $item['UserCity'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserPin']!="")
					  {
				     ?>
                    <li><b>Pin :</b> <?php echo $item['UserPin'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserLandLine']!="")
					  {
				     ?>
                    <li><b>LandLine :</b> <?php echo $item['UserLandLine'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserMobile']!="")
					  {
				     ?>
                    <li><b>Mobile :</b> <?php echo $item['UserMobile'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserEmail']!="")
					  {
				     ?>
                    <li><b>Email :</b> <?php echo $item['UserEmail'] ?></li>
                     <?php
					  }
					 ?>
                     <?php
					  if($item['UserService']!="")
					  {
				     ?>
                    <li><b>Product/Service Name :</b> <?php echo $item['ProductName'] ?></li>
                     <?php
					  }
					 ?>
                     </ul>
                    </p>
				</div>
			</div>
             <?php endforeach ?>
           <!-----profile view ends----->
		
	</div>
</section>
</div>

<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>
<script src="<?php echo base_url(); ?>assets/js/index1.js"></script>

    
    
    
  </body>
</html>
