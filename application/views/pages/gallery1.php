<?php require_once("sub_header.php");?>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url(); ?>assets/Flip_turn/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/Flip_turn/css/magnific-popup.css" rel="stylesheet"> 
	<link href="<?php echo base_url(); ?>assets/Flip_turn/css/templatemo_style.css" rel="stylesheet" type="text/css">	



 <a id="Gallary"></a>
  
        <div class="wow slideInRight">
            <div class="row gallary">
                <h3>Gallary Images</h3>
                <div id="portfolio-content" class="center-text">
			<div class="portfolio-page" id="page-1">
               <?php foreach ($gallery as $cat_item): ?>
				<div class="portfolio-group">
					<a class="portfolio-item" href="<?php echo base_url(); ?>assets/upload_galleryimg/<?php echo $cat_item['Image'] ?>">
						<img src="<?php echo base_url(); ?>assets/upload_galleryimg/<?php echo $cat_item['Image'] ?>" alt="image 1">
						<div class="detail">
							<h3 style="font-size:26px; padding-top:100px; text-transform:capitalize"><?php echo $cat_item['Title'] ?></h3>
							<!--<p>Duis ac laoreet mi. Maecenas non lorem sed elit molestie tincidunt. Vestibulum tincidunt libero urna, ut dignissim purus accumsan nec.</p>-->
							<span class="btn">View</span>
						</div>
					</a>				
				</div>
			   <?php endforeach ?>	
			</div>
		
		</div>
            </div>
        </div>
        
  
    
    
        
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Flip_turn/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Flip_turn/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Flip_turn/js/modernizr.2.5.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Flip_turn/js/jquery.magnific-popup.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Flip_turn/js/templatemo_script.js"></script>
	<script type="text/javascript">
		$(function () {
			$('.pagination li').click(changePage);
			$('.portfolio-item').magnificPopup({ 
				type: 'image',
				gallery:{
					enabled:true
				}
			});
		});
	</script>	        

<?php require_once("footer.php");?>