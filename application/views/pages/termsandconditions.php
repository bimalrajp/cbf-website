<?php require_once("sub_header.php"); ?>


    <div class="cd-container" style="align: center; width: 100%; height: auto; text-align: left; padding: 5%;">
        <h3 align="center" style="font-size: 32px; font-weight: bold;">Terms & Conditions</h3>
        <br/>
        <p align="center" style="font-size: 28px; font-weight: bold;">Catholic Business Forum Privacy Policy</p>
        <div style="width: 80%; height: 200px; min-height: 500px; overflow-y: scroll; padding: 10px; margin-top: 25px; margin-left: 10%; margin-right: 10%;">
            <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We Respect for
                your privacy and this is a non negotiable policy for us. We have just started but we have
                strong privacy principles in mind.
                CBF App provides messaging, Information Interchange using Images, Videos, File Attachments, and other
                services between Members. Our Privacy Policy helps explain our information (including message)
                practices..
                When we say “CBFApp,” “our,” “we,” or “us,” we’re talking about application developed for Catholic
                Business
                Forum by Colonialvision Consultancy Private Limited. This Privacy Policy (“Privacy Policy”) applies to
                all
                of our apps, services, features, software, and website (together, “Services”) unless specified
                otherwise.
            </p>
            <br/>
            <h1 style="color: #4b6c8f; font-size: 24px; font-weight: bold;">Information We Collect</h1>
            <br/>
            <p>
                CBFApp receives or collects information when we operate and provide our Services, including when you
                install, access, or use our Services.
            </p>
            <br>
            <div>
                <h5 style="font-weight: bold;">Information You Provide</h5>
                <ol>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Your Account Information. You will need to register with your details and business expertise
                        and other
                        data including mobile phone number to create a CBFApp account. You provide us the phone numbers
                        in your
                        mobile address book on a regular basis, including those of both the users of our Services and
                        your other
                        contacts. You confirm you are authorized to provide us such numbers. You may also add other
                        information
                        to your account, such as a profile name, profile picture, and status message.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Your Messages. We do not retain your messages in the ordinary course of providing our Services
                        to you.
                        Once your messages (including your chats, photos, videos, voice messages, files, and share
                        location
                        information) are delivered, they are deleted from our servers. Your messages are stored on your
                        own
                        device. If a message cannot be delivered immediately (for example, if you are offline), we may
                        keep it
                        on our servers for up to 30 days as we try to deliver it. If a message is still undelivered
                        after 30
                        days, we delete it. To improve performance and deliver media messages more efficiently, such as
                        when
                        many people are sharing a popular photo or video, we may retain that content on our servers for
                        a longer
                        period of time. We also offer end-to-end encryption for our Services, which is on by default,
                        when you
                        and the people with whom you message use a version of our app released after April 2, 2016.
                        End-to-end
                        encryption means that your messages are encrypted to protect against us and third parties from
                        reading
                        them.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Your Connections. To help you organize how you communicate with others, we may create a filter
                        to list
                        of your contacts on the basis of the filter for you.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Customer Support. You may provide us with information related to your use of our Services,
                        including
                        copies of your messages, and how to contact you so we can provide you customer support. For
                        example, you
                        may send us an email with information relating to our app performance or other issues.
                    </li>
                </ol>
            </div>
            <br>
            <div>
                <h5 style="font-weight: bold;">Automatically Collected Information</h5>
                <ol>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Usage and Log Information. We collect service-related, diagnostic, and performance
                        information. This includes information about your activity (such as how you use our Services,
                        how you interact with others using our Services, and the like), log files, and diagnostic,
                        crash, website, and performance logs and reports.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Device and Connection Information. We collect device-specific information when you install,
                        access, or use our Services. This includes information such as hardware model, operating system
                        information, browser information, IP address, mobile network information including phone number,
                        and device identifiers. We collect device location information if you use our location features,
                        such as when you choose to share your location with your contacts, view locations nearby or
                        those others have shared with you, and the like, and for diagnostics and troubleshooting
                        purposes such as if you are having trouble with our app’s location features.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Status Information. We collect information about delivery status of the messages you sent
                    </li>
                </ol>
            </div>
            <br>
            <div>
                <h5 style="font-weight: bold;">Third-Party Information</h5>
                <ol>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Third-Party Providers. We work with third-party providers to help us operate, provide,
                        improve, understand, customize, support, and market our Services. For example, we work with
                        companies to distribute our apps, provide our infrastructure, delivery, and other systems,
                        supply map and places information, help us understand how people use our Services, and market
                        our Services.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Third-Party Services. We allow you to use our Services in connection with third-party
                        services. If you use our Services with such third-party services, we may receive information
                        about you from them; Please note that when you use third-party services, their own terms and
                        privacy policies will govern your use of those services.
                    </li>
                </ol>
            </div>
            <br/>
            <h1 style="color: #4b6c8f; font-size: 24px; font-weight: bold;">How We Use Information</h1>
            <br>
            <div>
                <h5>We use all the information we have to help us operate, provide, improve, understand, customize,
                    support, and market our Services.</h5>
                <ol>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Our Services. We operate and provide our Services, including improving, fixing, and
                        customizing our Services. We understand how members use our Services, and analyze and use the
                        information we have to evaluate and improve our Services, research, develop, and test new
                        services and features, and conduct troubleshooting activities. We also use your information to
                        respond to you when you contact us.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Safety and Security. We verify accounts and activity, and promote safety and security on and
                        off our Services, such as by investigating suspicious activity or violations of our Terms, and
                        to ensure our Services are being used legally.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Third-Party Banner Ads. We do allow third-party banner ads on CBFApp. We have no intention to
                        introduce non Members, but if we ever do, we will update this policy.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Commercial Messaging. We will allow you and third parties, like businesses, to communicate
                        with each other using CBFApp, such as through order, transaction, and appointment information,
                        delivery and shipping notifications, product and service updates, and marketing.
                    </li>
                </ol>
            </div>
            <br/>
            <h1 style="color: #4b6c8f; font-size: 24px; font-weight: bold;">Information You And We Share</h1>
            <br>
            <div>
                <h5>You share your information as you use and communicate through our Services, and we share your
                    information to help us operate, provide, improve, understand, customize, support, and market our
                    Services.</h5>
                <ol>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Account Information. Your phone number, profile name and photo, online status and status
                        message, last seen status, and receipts may be available to anyone who uses our Services,
                        although you can configure your Services settings to manage certain information available to
                        other users.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Your Contacts and Others. Users with whom you communicate may store or reshare your
                        information (including your phone number or messages) with others on and off our Services. You
                        can use your Services settings and the block feature in our Services to manage the users of our
                        Services with whom you communicate and certain information you share.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Third-Party Providers. We work with third-party providers to help us operate, provide,
                        improve, understand, customize, support, and market our Services. When we share information with
                        third-party providers, we require them to use your information in accordance with our
                        instructions and terms or with express permission from you.
                    </li>
                </ol>
            </div>
            <br/>
            <h1 style="color: #4b6c8f; font-size: 24px; font-weight: bold;">Assignment, Change Of Control, And Transfer</h1>
            <br>
            <div>
                <h5>All of our rights and obligations under our Privacy Policy are freely assignable by us to any of our
                    affiliates, in connection with a merger, acquisition, restructuring, or sale of assets, or by
                    operation of law or otherwise, and we may transfer your information to any of our affiliates,
                    successor entities, or new owner.</h5>
            </div>
            <br/>
            <h1 style="color: #4b6c8f; font-size: 24px; font-weight: bold;">Managing Your Information</h1>
            <br>
            <div>
                <h5>If you would like to manage, change, limit, or delete your information, we allow you to do that
                    through the following tools:</h5>
                <ol>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Changing Your Mobile Phone Number, Profile Name and Picture. You must change your mobile phone
                        number using our in-app change number feature and transfer your account to your new mobile phone
                        number. You can also change your profile name, profile picture, and status message at any time.
                    </li>
                    <li style="margin: 10px; padding-left: 10px;">
                        • Deleting Your CBFApp Account. You may delete your CBFApp account at any time (including if you
                        want to revoke your consent to our use of your information) using our in-app delete my account
                        feature. When you delete your CBFApp account, your undelivered messages are deleted from our
                        servers as well as any of your other information we no longer need to operate and provide our
                        Services. Be mindful that if you only delete our Services from your device without using our
                        in-app delete my account feature, your information may be stored with us for a longer period.
                        Please remember that when you delete your account, it does not affect the information other
                        users have relating to you, such as their copy of the messages you sent them.
                    </li>
                </ol>
            </div>
            <br/>
            <h1 style="color: #4b6c8f; font-size: 24px; font-weight: bold;">Law And Protection</h1>
            <br>
            <div>
                <h5>We may collect, use, preserve, and share your information if we have a good-faith belief that it is
                    reasonably necessary to: (a) respond pursuant to applicable law or regulations, to legal process, or
                    to government requests; (b) enforce our Terms and any other applicable terms and policies, including
                    for investigations of potential violations; (c) detect, investigate, prevent, and address fraud and
                    other illegal activity, security, or technical issues; or (d) protect the rights, property, and
                    safety of our users.</h5>
            </div>
            <br/>
            <h1 style="color: #4b6c8f; font-size: 24px; font-weight: bold;">Updates To Our Policy</h1>
            <br>
            <div>
                <h5>We may amend or update our Privacy Policy. We will provide you notice of amendments to this Privacy
                    Policy, as appropriate, and update the “Last Modified” date at the top of this Privacy Policy. Your
                    continued use of our Services confirms your acceptance of our Privacy Policy, as amended. If you do
                    not agree to our Privacy Policy, as amended, you must stop using our Services. Please review our
                    Privacy Policy from time to time.</h5>
            </div>
            <br/>
            <h1 style="color: #4b6c8f; font-size: 24px; font-weight: bold;">Contact Us</h1>
            <br>
            <div>
                <h5>If you have questions about our Privacy Policy, please contact us.</h5>
                <br/>
                <p>Coloniavision Consultancy Private Limited.</p>
                <p>Legal & Privacy Policy Department </p>
                <p>A-8, First Floor, GCDA Complex,</p>
                <p>Marine Drive, Kochi- 682031</p>
            </div>
        </div>
    </div>

<?php require_once("footer.php"); ?>