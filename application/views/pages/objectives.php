<?php require_once("sub_header.php");?>

<div class="row" style="padding-bottom:10%;">


                <div class="wow bounceInUp">
                    <main>
        <!--<a name="WhoWeAre"></a>-->
          <a id="WhoWeAre"></a>
		<section  class="cd-section whoweare">
			<div class="cd-container" >
				<h3>Aims and Objectives</h3>
			    <ol class="objectives" >
            	<li>Networking to establish cooperation among catholic business houses.</li>
            	<li>Faster relationship with Government machinery.</li>
                <li>Publish a monthly Business Magazine consolidating information about trade arena.</li>
                <li>Publicize and encourage maximum participation of general public in programs organized by CBF.</li>
                <li>Convening seminars, conferences, symposiums, discussions and debates.</li>
                <li>Identifying and coordinating with eminent resource persons in trade circles.</li>
                <li>Rapport with business communities beyond boundaries of culture, creed and language.</li>
                <li>Offer updated information about trade and industry-fairs,Exhibitions.</li>
           </ol>
			</div> <!-- cd-container -->
		</section> <!-- cd-team -->

	</main>
                </div>


            </div>

<?php require_once("footer.php");?>