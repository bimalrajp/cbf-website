<?php require_once("sub_header.php");?>

<div class="row" style="padding-bottom:10%;">


                <div class="wow bounceInUp">
                    <main>
        <!--<a name="WhoWeAre"></a>-->
          <!--<a id="WhoWeAre"></a>-->
		<section id="cd-team" class="cd-section whoweare">
			<div class="cd-container" >
				<h3>Who We Are</h3>
				<p style="font-size:14px; color:#666; text-align:justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Talents vary yet all of us have an equal opportunity to develop our talents. Catholic Business Forum has taken a pioneer step to use the synergy of our collective knowledge and resources in the field of business..CBF is founded on the belief that ideas are “Nobody’s Monopoly people who’s think big, think fast, think ahead” are taking the community to greater heights. CBF was born with a vision to come together as a beginning; to strive together for progress; work together for success.</p>
                
                <p style="font-size:14px; color:#666; text-align:justify">CBF was inaugurated on 24th September 2015 by His Excellency Mar Sebastian Adayantharath. At present almost 500 members actively participate in the activities of CBF. Two meetings on 13th February 2016 and 25th April 2016 were conducted respectively.</p>
                
                <p style="font-size:14px; color:#666; text-align:justify">Naipunnya International, promoted by the Archdiocese of Ernakulum - Angamaly, is located in Cochin. We are providing training in Communicative English, Soft skills development, IELTS & Foreign Languages. We are also sending students for higher studies abroad. In addition, we provide Civil Services Examination Coaching, Bank Examination Coaching, etc…Naipunnya International has taken the initiative to organize CBF as its commitment to the Business Community.</p>
                
               
			</div> <!-- cd-container -->
		</section> <!-- cd-team -->

	</main>
                </div>


            </div>

<?php require_once("footer.php");?>