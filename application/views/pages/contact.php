<?php require_once("sub_header.php"); ?>

<div class="contact_us">
    <div class="wow swing center animated">
        <h3>Get In Touch</h3>
        <address>
            CATHOLIC BUSINESS FORUM<br/>
            Major Archbishop's House Campus,<br/>
            Broadway, Ernakulam - 682 031<br/>
            Kerala, India<br/>
            cbf@naipunnyaint.com<br/>
            Off No. +91-484-2381948/49<br/>
        </address>
    </div>
</div>

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15717.676165243724!2d76.2759983!3d9.9822019!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb060df59209bfef0!2sMajor+Archbishop&#39;s+House!5e0!3m2!1sen!2sin!4v1466242247342"
        width="90%" height="400" frameborder="2px" style="margin: 5%" allowfullscreen></iframe>
<div style="width: 100%; height: 30px; padding-right: 10px;">
    <a style="color: #4b6c8f; text-decoration: none;" class="pull-right" href="<?php echo base_url(); ?>termsandconditions">Terms & Conditions</a>
</div>

<?php require_once("footer.php"); ?>
