<?php require_once("sub_header.php");?>
<!---------gallery style starts----------->
<link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
<style>
.polaroid {
  position: relative;
  width: 220px;
}

.polaroid img {
  border: 10px solid #fff;
  border-bottom: 45px solid #fff;
 -webkit-box-shadow: 7px 9px 31px 6px rgba(163,155,163,1);
-moz-box-shadow: 7px 9px 31px 6px rgba(163,155,163,1);
box-shadow: 7px 9px 31px 6px rgba(163,155,163,1);
}

.polaroid p {
  position: absolute;
  text-align: center;
  width: 100%;
  bottom: 10px;
  font: 400 18px/1 'Kaushan Script', cursive;
  color: #888;
}
</style>
<!-----------gallery style ends----------->

<div class="row" style="padding-bottom:10%;">


                <div class="wow bounceInUp">
                    <main>
        <!--<a name="WhoWeAre"></a>-->
          <!--<a id="WhoWeAre"></a>-->
		<section id="cd-team" class="cd-section whoweare">
			<div class="cd-container" >
				<h3>Gallary At Glance</h3>
			
<?php foreach ($gallery as $cat_item): ?>
   <a href="<?php echo base_url("/gallery1/".$cat_item['GalleryID']); ?>" title="<?php echo $cat_item['GalleryName'] ?>">
   <div class="polaroid">
 	 <p><?php echo $cat_item['GalleryName'] ?></p>
 	 <img src="<?php echo base_url(); ?>assets/upload_gallery/<?php echo $cat_item['Logo'] ?>" />
  </div>
  </a>
<?php endforeach?>              
                
                
               
			</div> <!-- cd-container -->
		</section> <!-- cd-team -->

	</main>
                </div>


            </div>

<?php require_once("footer.php");?>