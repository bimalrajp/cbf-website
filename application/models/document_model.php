<?php
class Document_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}
	
	public function set_document()
	{
		$data=array(
			'DocumentTitle'=>$this->input->post('document_name'),
			);
		return $this->db->insert('document', $data);
		
	}

	
	public function set_documentdoc($id)
	{
		$sql = "SELECT max(DocID) as id from documentdoc";
		$result = mysql_query($sql)or die(mysql_error());
		while($row1=mysql_fetch_array($result))
		{
		 $rid=$row1['id']+1;		
		$DocumentOrg=$_FILES['document_doc']['name'];
		$ext=array_pop(explode('.', $DocumentOrg));
		/*if($ext=='docx')
		{
			$ext='doc';
		}*/
		$DocumentDoc="DocumentDoc".$rid.".".$ext;
		move_uploaded_file($_FILES["document_doc"]["tmp_name"], "assets/upload_documentdoc/".$DocumentDoc);
		$data=array(
			'DocumentID	'=>$id,
			'DocTitle'=>$this->input->post('document_name'),
			'Doc'=>$DocumentDoc
			);
		return $this->db->insert('documentdoc', $data);
	}
	}
	
	public function edit_document($did)
	{
		
		$data = array(
			'DocumentTitle'=>$this->input->post('document_name')
		);
		$this->db->where('DocumentID', $did);
		return $this->db->update('document', $data); 
	}
	
	
    public function get_document()
	{
		$query=$this->db->query('SELECT * FROM document order by DocumentID desc');
		return $query->result_array();
	}
	
	public function get_documentdoc()
	{
		$query=$this->db->get('documentdoc');
		return $query->result_array();
	}
	
	public function get_documentbyid($id)
	{
		$query=$this->db->get_where('document', array('DocumentID'=>$id));
		return $query->result_array();
	}

	public function get_documentdocbyid($id)
	{
		$query=$this->db->get_where(' documentdoc', array('DocumentID'=>$id));
		return $query->result_array();
	}
	

	public function dlt_documentdoc($path,$did)
	{
		//unlink($path);
		$query=$this->db->delete('documentdoc', array('DocID' => $did));
	}
	public function dlt_document($id='')
	{
	if($id!='')
		{
		  $query=$this->db->delete('document', array('DocumentID' => $id));
		}
}
}


?>