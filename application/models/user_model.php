<?php
class User_model extends CI_Model
{
	public function Login($username, $password)
	{
		$rc=0;
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('UserName', $username);
		$this->db->limit(1);
		$query=$this->db->get();
		$rc=$query->num_rows();
		if($rc==1)
		{
			foreach($query->result() as $row)
			{
				$DBPassword=$row->Password;
				$pass = $this->encrypt->decode($DBPassword);
				if($pass==$password)
				{
					$loginResponse = $query->result();
					return $loginResponse;
				}
				else
				{
					$loginResponse = 2;
					return $loginResponse;
				}
			}
		}
		else
		{
			return 3;
		}
	}

	public function set_user()
	{
		/*$SelectCompany = $this->input->post('SelectCompany');
		if($SelectCompany=='other')
		{*/
			$CompanyName = str_replace("'","`",$this->input->post('CompanyName'));
			if($CompanyName!='')
			{
				$CompanyData=array(
					'CompanyName' => $CompanyName
				);
				$r1= $this->db->insert('companies', $CompanyData);
				$UserCompanyName = $this->db->insert_id();
			}
		/*}*/
		//$SelectProduct = str_replace("'","`",$this->input->post('SelectProduct'));
		/*if($SelectProduct=='other')
		{*/
			$ProductName = str_replace("'","`",$this->input->post('ProductName'));
			if($ProductName!='')
			{
				$ProductData=array(
					'ProductName' => $ProductName
				);
				$r2= $this->db->insert('products', $ProductData);
				$UserService = $this->db->insert_id();
			}
		/*}*/
		$pass = $this->encrypt->encode($this->input->post('RPassword'));
		$photo='';
		$filename=$_FILES['UserImage']['name'];
		if($filename!="")
		{
			$ext_temp=explode('.', $filename);
			$ext=end($ext_temp);
			$word = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
			shuffle($word);
			$filename1= substr(implode($word), 0, 10);
			//$posttimemod=preg_replace("[:]", "", $posttime);
			$photo = $filename1.'.'.$ext;
			move_uploaded_file($_FILES["UserImage"]["tmp_name"], "assets/user/" .$photo);
		}
		$data=array(
			'UserFullName' => str_replace("'","`",$this->input->post('Name')),
			'UserType' => 0,
			'UserFlag' => 1,
			'UserGender' => $this->input->post('Gender'),
			'UserMobile' => $this->input->post('Mobile'),
			'UserEmail' => $this->input->post('Email'),
			'UserCompanyName' => $UserCompanyName,
			'UserService' => $UserService,
			'UserDescription' => str_replace("'","`",$this->input->post('UserDescription')),
			'UserName' => $this->input->post('RUserName'),
			'Password' => $pass,
			'UserImage' => $photo
		);
		$r=$this->db->insert('user', $data);
		$_SESSION["PKUserID"]=$this->db->insert_id();
		$_SESSION['username']=$this->input->post('RUserName');
		$_SESSION["UserType"]=0;
		$CreatedToken=$this->Token($_SESSION["PKUserID"],$_SESSION['username'],'web',$_SESSION["UserType"]);
		foreach($CreatedToken as $row1)
		{
			$_SESSION['Token']=$row1->Token;
		}
		return $r;
	}

	public function get_profile($id='')
	{
		if($id=='')
		{
			if(isset($_SESSION['PKUserID']))
			{
				$id=$_SESSION['PKUserID'];
			}
		}
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('companies', 'user.UserCompanyName =companies.PKCompanyID','left');
		$this->db->join('products', 'user.UserService =products.PKProductID','left');
		$this->db->where('PKUserID', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_directory()
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('companies', 'user.UserCompanyName =companies.PKCompanyID','left');
		$this->db->join('products', 'user.UserService =products.PKProductID','left');
		$this->db->where('UserType',0);
		$this->db->where('UserFlag',1);
		$this->db->order_by("UserFullName","asc");
		$query = $this->db->get();
		return $query->result_array();
	}

	public function update_password($a='')
	{
		if($a=='web')
		{
		}
		if($a=='app')
		{
			$Token = $_POST['Token'];
			$res=$this->user_model->TokenTimeExpirationCheck($Token);
			if($res==1)
			{
				$PKUserID=$_POST['PKUserID'];
				$CurrentPassword=$_POST['CurrentPassword'];
				$NewPassword=$_POST['NewPassword'];
				
				$userdetails=$this->get_profile($PKUserID);
				foreach($userdetails as $ud)
				{
					$CurrentPasswordInDB=$this->encrypt->decode($ud['Password']);
				}
				if($CurrentPasswordInDB!=$CurrentPassword)
				{
					$chatmsgresp['CurrentPasswordFlag']=0;
					$chatmsgresp['PasswordUpdateFlag']=0;
				}
				else
				{
					$chatmsgresp['CurrentPasswordFlag']=1;
					$data=array(
						'Password' => $this->encrypt->encode($NewPassword)
					);
					$this->db->where('PKUserID', $PKUserID);
					$r= $this->db->update('user', $data);
					if($r==1)
					{
						$chatmsgresp['PasswordUpdateFlag']=1;
					}
					else
					{
						$chatmsgresp['PasswordUpdateFlag']=0;
					}
				}
				
				$chatmsgresp['TokenFlag']=1;
			}
			else if($res==2)
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=2;
				$chatmsgresp['CurrentPasswordFlag']=0;
				$chatmsgresp['PasswordUpdateFlag']=0;
			}
			else if($res==0)
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=0;
				$chatmsgresp['CurrentPasswordFlag']=0;
				$chatmsgresp['PasswordUpdateFlag']=0;
			}
			return $chatmsgresp;
		}
	}

	public function update_profile($a='')
	{
		if($a=='web')
		{
			$PKUserID=$_SESSION['PKUserID'];
			//$UserImage=$this->input->post('photo');
			$filename=$_FILES['photo']['name'];
			if($filename!="")
			{
				$tempfilename=explode('.', $filename);
				$ext=array_pop($tempfilename);
				$word = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
				shuffle($word);
				$filename1= substr(implode($word), 0, 10);
				//$posttimemod=preg_replace("[:]", "", $posttime);
				$UserImage = $filename1.'.'.$ext;
				move_uploaded_file($_FILES["photo"]["tmp_name"], "assets/user/" .$UserImage);
			}
			if($filename =="")
			{
				$UserImage=$this->input->post('photo1');
			}
			$UserCompanyID=$this->input->post('UserCompanyID');
			$UserCompanyName=$this->input->post('UserCompanyName');
			$UserServiceID=$this->input->post('UserServiceID');
			$UserService=$this->input->post('UserService');
			$UserFullName=$this->input->post('UserFullName');
			$UserHouseName=$this->input->post('UserHouseName');
			$UserGender=$this->input->post('UserGender');
			$UserAddress=$this->input->post('UserAddress');
			$UserCity=$this->input->post('UserCity');
			$UserPin=$this->input->post('UserPin');
			$UserLandLine=$this->input->post('UserLandLine');
			$UserMobile=$this->input->post('UserMobile');
			$UserEmail=$this->input->post('UserEmail');
			$UserDescription=$this->input->post('UserDescription');
			
			$datacompany = array(
				'CompanyName' => $UserCompanyName
			);
			if($UserCompanyID!=0)
			{
				$this->db->where('PKCompanyID', $UserCompanyID);
				$this->db->update('companies', $datacompany);
			}
			else
			{
				$this->db->insert('companies', $datacompany);
				$UserCompanyID = $this->db->insert_id();
			}
			$dataservice = array(
				'ProductName' => $UserService
			);
			if($UserServiceID!=0)
			{
				$this->db->where('PKProductID', $UserServiceID);
				$this->db->update('products', $dataservice);
			}
			else
			{
				$this->db->insert('products', $dataservice);
				$UserServiceID = $this->db->insert_id();
			}
			$data = array(
				'UserImage' => $UserImage,
				'UserFullName'=>$UserFullName,
				'UserHouseName'=>$UserHouseName,
				'UserGender'=>$UserGender,
				'UserAddress'=>$UserAddress,
				'UserCity'=>$UserCity,
				'UserPin'=>$UserPin,
				'UserLandLine'=>$UserLandLine,
				'UserMobile'=>$UserMobile,
				'UserEmail'=>$UserEmail,
				'UserCompanyName'=>$UserCompanyID,
				'UserService' => $UserServiceID,
				'UserDescription'=>$UserDescription
			);
			$this->db->where('PKUserID', $PKUserID);
			return $this->db->update('user', $data);
		}
		else if($a=='app')
		{
			$Token = $_POST['Token'];
			$res=$this->user_model->TokenTimeExpirationCheck($Token);
			if($res==1)
			{
				$PKUserID=$_POST['PKUserID'];
				$userdetails=$this->get_profile($PKUserID);
				//$UserImage=$this->input->post('UserImage');
				if(isset($_FILES['UserImage']))
				{
					$filename=$_FILES['UserImage']['name'];
					if($filename!="")
					{
						$tempfilename=explode('.', $filename);
						$ext=array_pop($tempfilename);
						$word = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
						shuffle($word);
						$filename1= substr(implode($word), 0, 10);
						//$posttimemod=preg_replace("[:]", "", $posttime);
						$UserImage = $filename1.'.'.$ext;
						move_uploaded_file($_FILES['UserImage']['tmp_name'], "assets/user/" .$UserImage);
					}
				}
				//if($filename=="")
				else
				{
					$UserImage=$this->input->post('PreUserImage');
				}
				$UserFullName=$_POST['UserFullName'];
				$UserPin=$_POST['UserPin'];
				$UserLandLine=$_POST['UserLandLine'];
				foreach($userdetails as $ud)
				{
					$UserCompanyID=$ud['UserCompanyName'];
					$UserServiceID=$ud['UserService'];
					if($UserPin=="")
					{
						$UserPin=$ud['UserPin'];
					}
					if($UserLandLine=="")
					{
						$UserLandLine=$ud['UserLandLine'];
					}
				}
				$UserCompanyName=$_POST['UserCompanyName'];
				$UserService=$_POST['UserService'];
				$UserHouseName=$_POST['UserHouseName'];
				$UserGender=$_POST['UserGender'];
				$UserAddress=$_POST['UserAddress'];
				$UserCity=$_POST['UserCity'];
				$UserMobile=$_POST['UserMobile'];
				$UserEmail=$_POST['UserEmail'];
				$UserDescription=$_POST['UserDescription'];
				
				$datacompany = array(
					'CompanyName' => $UserCompanyName
				);
				if($UserCompanyID!=0)
				{
					$this->db->where('PKCompanyID', $UserCompanyID);
					$this->db->update('companies', $datacompany);
				}
				else
				{
					$this->db->insert('companies', $datacompany);
					$UserCompanyID = $this->db->insert_id();
				}
				$dataservice = array(
					'ProductName' => $UserService
				);
				if($UserServiceID!=0)
				{
					$this->db->where('PKProductID', $UserServiceID);
					$this->db->update('products', $dataservice);
				}
				else
				{
					$this->db->insert('products', $dataservice);
					$UserServiceID = $this->db->insert_id();
				}
				$data = array(
					'UserImage' => $UserImage,
					'UserFullName'=>$UserFullName,
					'UserHouseName'=>$UserHouseName,
					'UserGender'=>$UserGender,
					'UserAddress'=>$UserAddress,
					'UserCity'=>$UserCity,
					'UserPin'=>$UserPin,
					'UserLandLine'=>$UserLandLine,
					'UserMobile'=>$UserMobile,
					'UserEmail'=>$UserEmail,
					'UserCompanyName'=>$UserCompanyID,
					'UserService' => $UserServiceID,
					'UserDescription'=>$UserDescription
				);
				$this->db->where('PKUserID', $PKUserID);
				$r=$this->db->update('user', $data);
				if($r==1)
				{
					$chatmsgresp['ProfileUpdateFlag']=1;
					$chatmsgresp['ErrorFlag']=0;
				}
				else if($r==0)
				{
					$chatmsgresp['ProfileUpdateFlag']=0;
					$chatmsgresp['ErrorFlag']=1;
				}
				$chatmsgresp['TokenFlag']=1;
			}
			else if($res==2)
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=2;
				$chatmsgresp['ProfileUpdateFlag']=0;
			}
			else if($res==0)
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=0;
				$chatmsgresp['ProfileUpdateFlag']=0;
			}
			return $chatmsgresp;
		}
	}

	function Token($UID,$UN,$tokentype,$logintype)
	{
		$res='';
		$TokenType=0;
		$LoginTime=date('Y-m-d H:i:s');
		$Token=urlencode($this->encrypt->encode($UID.$UN.$LoginTime));

		if($tokentype=='web')
		{
			$TokenType=1;
		}
		else if($tokentype=='app')
		{
			$TokenType=2;
		}
		$data = array
		(
			'FKUserID' => $UID,
			'Token' => $Token,
			'LoginType' => $logintype,
			'TokenType' => $TokenType,
			'LoginFlag' => 1,
			'LoginTime' => $LoginTime,
			'LastActiveTime' => $LoginTime
		);
		$res=$this->db->insert('logintokens', $data);

		if($res==1)
		{
			return $this->get_Token_Details($Token);
		}
	}

	public function get_Token_Details($Token='')
	{
		$this->db->select('*');
		$this->db->from('logintokens');
		$this->db->where('Token', $Token);
		$this->db->limit(1);
		$query=$this->db->get();
		return $query->result();
	}

	function TokenTimeExpirationCheck($Token)
	{
		$ExpirationDays=30;
		$ExpirationSecs=5;
		$UpdatedOn=date('Y-m-d H:i:s');
		$this->db->select('LastActiveTime');
		$this->db->from('logintokens');
		$this->db->where('Token', $Token);
		$this->db->where('LoginFlag', 1);
		$this->db->limit(1);
		$query=$this->db->get();

		if($query->num_rows()==1)
		{
			foreach($query->result() as $row)
			{
				$LastTime=$row->LastActiveTime;
			}
			/*$TimeDifference=round(abs(strtotime($UpdatedOn) - strtotime($LastTime))/86400,2);*/
			$TimeDifference=intval(abs(strtotime($UpdatedOn) - strtotime($LastTime))/86400);
			//$TimeDifference=intval(abs(strtotime($UpdatedOn) - strtotime($LastTime)));

			if($TimeDifference<=$ExpirationDays)
			{
				$data = array
				(
					'LastActiveTime' => $UpdatedOn
				);
				$this->db->where('Token', $Token);
				$this->db->update('logintokens', $data);
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 2;
		}
	}

	function TokenChangeOnLogout($Token)
	{
		$AutoLogout=0;
		$UpdatedOn=date('Y-m-d H:i:s');
		$TTE=$this->TokenTimeExpirationCheck($Token);
		if($TTE==1||$TTE==0)
		{
			if($TTE==0)
			{
				$AutoLogout=1;
			}
			$data = array
			(
				'LogoutTime' => $UpdatedOn,
				'LoginFlag' => 0,
				'AutoLogout' => $AutoLogout
			);
			$this->db->where('Token', $Token);
			$r=$this->db->update('logintokens', $data);
			if($r==true)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 2;
		}
	}
}
?>