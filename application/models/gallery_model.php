<?php
class Gallery_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}
	
	public function set_gallery()
	{
		$sql = "SELECT max(GalleryID) as id from gallery";
		$result = mysql_query($sql)or die(mysql_error());
		while($row1=mysql_fetch_array($result))
		{
		 $id=$row1['id']+1;		
		$GalleryImageOrg=$_FILES['gallery_img']['name'];
		$ext=array_pop(explode('.', $GalleryImageOrg));
		$GalleryImage="GalleryImage".$id.".".$ext;
		move_uploaded_file($_FILES["gallery_img"]["tmp_name"], "assets/upload_gallery/".$GalleryImage);
	    $data=array(
			'Logo'=>$GalleryImage,
			'GalleryName'=>$this->input->post('gallery_name'),
			);
		return $this->db->insert('gallery', $data);
		}
	}

	
	public function set_galleryimg($id)
	{
		$sql = "SELECT max(ImgID) as id from galleryimages";
		$result = mysql_query($sql)or die(mysql_error());
		while($row1=mysql_fetch_array($result))
		{
		 $rid=$row1['id']+1;		
		$GalleryImageOrg=$_FILES['gallery_img']['name'];
		$ext=array_pop(explode('.', $GalleryImageOrg));
		$GalleryImage="GalleryImg".$rid.".".$ext;
		move_uploaded_file($_FILES["gallery_img"]["tmp_name"], "assets/upload_galleryimg/".$GalleryImage);
		$data=array(
			'GalleryID'=>$id,
			'Title'=>$this->input->post('gallery_name'),
			'Image'=>$GalleryImage
			);
		return $this->db->insert('galleryimages', $data);
	}
	}
	
	public function edit_gallery($gid)
	{
		$filename=$_FILES['gallery_img']['name'];
		if($filename!="")
		{
			$ext=array_pop(explode('.', $filename));
			$word = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
			shuffle($word);
			$filename1= substr(implode($word), 0, 10);
			//$posttimemod=preg_replace("[:]", "", $posttime);
			$photo = $filename1.'.'.$ext;
			move_uploaded_file($_FILES["gallery_img"]["tmp_name"], "assets/upload_gallery/".$photo);
		}
		if($filename =="")
		{
			$photo=$this->input->post('gallery_img1');
		}
		$data = array(
			'Logo' => $photo,
			'GalleryName'=>$this->input->post('gallery_name')
		);
		$this->db->where('GalleryID', $gid);
		return $this->db->update('gallery', $data); 
	}
	
	
    public function get_gallery()
	{
		$query=$this->db->query('SELECT * FROM gallery ');
		return $query->result_array();
	}
	
	public function get_gallerybyid($id)
	{
		$query=$this->db->get_where('gallery', array('GalleryID'=>$id));
		return $query->result_array();
	}

	public function get_galleryimgsbyid($id)
	{
		$query=$this->db->get_where('galleryimages', array('GalleryID'=>$id));
		return $query->result_array();
	}
	
	public function get_gallery_desc()
	{

	/*$this->db->select('*');
	$this->db->from('gallery');
	$this->db->order_by("gallery_id", "desc");*/
	  $query=$this->db->query('SELECT * FROM gallery order by GalleryID desc');
	/*$query = $this->db->get();*/
    return $query->result();
}
	
	public function get_galleryimgbyid($id)
	{
		$query=$this->db->get_where('galleryimages', array('ImgID'=>$id));
		return $query->result_array();
	}
	public function dlt_galleryimgs($path,$iv_id)
	{
		//unlink($path);
		$query=$this->db->delete('galleryimages', array('ImgID' => $iv_id));
	}
	public function dlt_gallery($path='',$pathgi='',$id='')
	{
	if($id!='')
		{
			//unlink($pathgi);
			$imgs=$this->get_galleryimgsbyid($id);
			if($imgs!='')
			{
				foreach($imgs as $i):
				unlink($path.$i['Image']);
				endforeach;
				$query=$this->db->delete('galleryimages', array('GalleryID' => $id));
			}
			//unlink($path.'/*.*');
			$query=$this->db->delete('gallery', array('GalleryID' => $id));
			$this->dlt_gallery($path);
		}
}
}


?>