<?php
class Admin_model extends CI_Model
{

	public function get_profile()
	{
		$id=$_SESSION['PKUserID'];
		/*$id=1;*/
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('companies', 'user.UserCompanyName =companies.PKCompanyID','left');
		$this->db->join('products', 'user.UserService =products.PKProductID','left');
		$this->db->where('PKUserID', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_profile_user($id)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('companies', 'user.UserCompanyName =companies.PKCompanyID','left');
		$this->db->join('products', 'user.UserService =products.PKProductID','left');
		$this->db->where('PKUserID', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_users()
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('companies', 'user.UserCompanyName =companies.PKCompanyID','left');
		$this->db->join('products', 'user.UserService =products.PKProductID','left');
		$this->db->where('UserType', '0');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_updated_user($id)
	{
		$this->db->select('PKUserID,UserFlag,UserMessageFlag,UserFullName,UserGender,UserHouseName,UserAddress,UserCity,UserPin,UserMobile,UserEmail,UserDescription,UserImage,CompanyName,ProductName,ProductDescreption');
		$this->db->from('user');
		$this->db->join('companies', 'user.UserCompanyName =companies.PKCompanyID','left');
		$this->db->join('products', 'user.UserService =products.PKProductID','left');
		//$this->db->where('UserType', '0');
		$this->db->where('PKUserID',$id);
		$query = $this->db->get();
		if($query->num_rows()>0){
			$chatmsgresp=$query->result_array();
		}
		return $chatmsgresp;
	}

	public function get_users_list()
	{
		$Token = $_POST['Token'];
		$MaxID = $_POST['MaxID'];
		$res=$this->user_model->TokenTimeExpirationCheck($Token);
		if($res==1)
		{
			$chatmsgresp['TokenFlag']=1;
			$chatmsgresp['ErrorFlag']=0;

			$this->db->select('PKUserID,UserFlag,UserMessageFlag,UserFullName,UserGender,UserHouseName,UserAddress,UserCity,UserPin,UserMobile,UserEmail,UserDescription,UserImage,CompanyName,ProductName,ProductDescreption');
			$this->db->from('user');
			$this->db->join('companies', 'user.UserCompanyName =companies.PKCompanyID','left');
			$this->db->join('products', 'user.UserService =products.PKProductID','left');
			//$this->db->where('UserType', '0');
			$this->db->where('PKUserID >',$MaxID);
			$query = $this->db->get();
			$chatmsgresp['ContactsCount']=$query->num_rows();
			if($chatmsgresp['ContactsCount']>0){
				$chatmsgresp['Contacts']=$query->result_array();
			}
		}
		else if($res==2)
		{
			$chatmsgresp['ErrorFlag']=1;
			$chatmsgresp['TokenFlag']=2;
		}
		else if($res==0)
		{
			$chatmsgresp['ErrorFlag']=1;
			$chatmsgresp['TokenFlag']=0;
		}
		return $chatmsgresp;
	}

	public function profile_view($id)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('companies', 'user.UserCompanyName =companies.PKCompanyID','left');
		$this->db->where('PKUserID', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_news()
	{
		$this->db->select('*');
		$this->db->from('news');
		$this->db->order_by("PKNewsID", "asc");
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_single_news($id)
	{
		$this->db->select('*');
		$this->db->from('news');
		$this->db->where('PKNewsID', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function set_news()
	{
		$data=array(
			'News'=>$this->input->post('news'),
			);
		return $this->db->insert('news', $data);
		
	}

	public function news_edit($id)
	{
		$data=array(
			'News'=>$this->input->post('news'),
			);
		$this->db->where('PKNewsID', $id);
		return $this->db->update('news', $data);
		
	}

	public function news_delete($id)
	{
		return $query=$this->db->delete('news', array('PKNewsID' => $id));
	}

	public function get_company()
	{
		$this->db->distinct();
		$this->db->select('*');
		$this->db->from('companies');
		$this->db->where('CompanyName !=', '');
		$this->db->order_by("CompanyName", "asc");
		$query = $this->db->get();
		return $query->result_array();
	}

	public function update_user_profile($id)
	{
		$filename=$_FILES['photo']['name'];
		if($filename!="")
		{
			$ext=array_pop(explode('.', $filename));
			$word = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
			shuffle($word);
			$filename1= substr(implode($word), 0, 10);
			//$posttimemod=preg_replace("[:]", "", $posttime);
			$photo = $filename1.'.'.$ext;
			move_uploaded_file($_FILES["photo"]["tmp_name"], "assets/user/" .$photo);
		}
		if($filename =="")
		{
			$photo=$this->input->post('photo1');
		}
		$data = array(
			'UserImage' => $photo,
			'UserFullName'=>$this->input->post('UserFullName'),
			'UserHouseName'=>$this->input->post('UserHouseName'),
			'UserGender'=>$this->input->post('UserGender'),
			'UserAddress'=>$this->input->post('UserAddress'),
			'UserCity'=>$this->input->post('UserCity'),
			'UserPin'=>$this->input->post('UserPin'),
			'UserLandLine'=>$this->input->post('UserLandLine'),
			'UserMobile'=>$this->input->post('UserMobile'),
			'UserEmail'=>$this->input->post('UserEmail'),
			'UserCompanyName'=>$this->input->post('UserCompanyName'),
			'UserService'=>$this->input->post('UserService'),
			'UserName'=>$this->input->post('UserName'),
			'Password'=>$this->encrypt->encode($this->input->post('Password')),
			'UserDescription'=>$this->input->post('UserDescription')
		 );
		$this->db->where('PKUserID', $id);
		return $this->db->update('user', $data); 
	}

	public function get_products()
	{
		$this->db->select('*');
		$this->db->from('products');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_name()
	{
		//$this->db->distinct();
		$this->db->select('UserFullName');
		$this->db->from('user');
		$this->db->where('UserType', '0');
		$query = $this->db->get();
		return $query->result_array();
	}	

	public function approve_user($id,$a='web')
	{
		if($a=='web')
		{
			$flag=$this->input->post('UserFlag');
			if($flag=='1')
			{
				$flag=0;
			}
			else
			{
				$flag=1;
			}
			$data = array(
				'UserFlag' =>$flag
			);
			$this->db->where('PKUserID', $id);
			$this->db->where('UserType', '0');
			return $this->db->update('user', $data);
		}
		if($a=='app')
		{
			$Token = $_POST['Token'];
			$UserID = $_POST['UserID'];
			$Approve = $_POST['Approve'];
			$res=$this->user_model->TokenTimeExpirationCheck($Token);
			if($res==1)
			{
				$chatmsgresp['TokenFlag']=1;
	
				if($id=='profile')
				{
					$data = array(
						'UserFlag' =>$Approve
					);
				}
				if($id=='message')
				{
					$data = array(
						'UserMessageFlag' =>$Approve
					);
				}
				$this->db->where('PKUserID', $UserID);
				$this->db->where('UserType', '0');
				$res = $this->db->update('user', $data);
				if($res==1)
				{
					$chatmsgresp['ApprovalFlag']=1;
					$chatmsgresp['ErrorFlag']=0;
				}
				else
				{
					$chatmsgresp['ApprovalFlag']=0;
					$chatmsgresp['ErrorFlag']=1;
				}
			}
			else if($res==2)
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=2;
				$chatmsgresp['Approval']=0;
			}
			else if($res==0)
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=0;
				$chatmsgresp['Approval']=0;
			}
			return $chatmsgresp;
		}
	}

	public function update_profile($id='')
	{
		/*$id = 1;*/
		$id=$_SESSION['PKUserID'];
		$photo=$this->input->post('photo');
		$filename=$_FILES['photo']['name'];
		if($filename!="")
		{
			$ext=array_pop(explode('.', $filename));
			$word = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
			shuffle($word);
			$filename1= substr(implode($word), 0, 10);
			//$posttimemod=preg_replace("[:]", "", $posttime);
			$photo = $filename1.'.'.$ext;
			move_uploaded_file($_FILES["photo"]["tmp_name"], "assets/user/" .$photo);
		}
		if($filename =="")
		{
			$photo=$this->input->post('photo1');
		}
		$data = array(
			'UserImage' => $photo,
			'UserFullName'=>$this->input->post('UserFullName'),
			'UserHouseName'=>$this->input->post('UserHouseName'),
			'UserGender'=>$this->input->post('UserGender'),
			'UserAddress'=>$this->input->post('UserAddress'),
			'UserCity'=>$this->input->post('UserCity'),
			'UserPin'=>$this->input->post('UserPin'),
			'UserLandLine'=>$this->input->post('UserLandLine'),
			'UserMobile'=>$this->input->post('UserMobile'),
			'UserEmail'=>$this->input->post('UserEmail'),
			'UserCompanyName'=>$this->input->post('UserCompanyName'),
			'UserService'=>$this->input->post('UserService'),
			'UserDescription'=>$this->input->post('UserDescription')
		);
		$this->db->where('PKUserID', $id);
		return $this->db->update('user', $data); 
	}

	public function send_msg()
	{
		//$this->db->distinct();
		$mob=$this->input->post('users');
		$Msg=$this->input->post('UserMessage');
		$msg=preg_replace('/\s+/', '%20', $Msg);
		file_get_contents("http://smsc.a4add.com/api/smsapi.aspx?username=mypolicesms&password=iamsuperman&to=".$mob."&from=mmpapp&message=".$msg);
	}

	public function search($a='web')
	{
		if($a=='web')
		{
			$name=$this->input->post('name');
			$company_name=$this->input->post('company_name');
			$product_name=$this->input->post('product_name');
			$this->db->select('*');
			$this->db->from('user');
			$this->db->join("companies","user.UserCompanyName = companies.PKCompanyID");
			$this->db->join("products","user.UserService = products.PKProductID");
			if($company_name!='')
			{
				$this->db->like('CompanyName', $company_name);
			}
			if($name!='')
			{
				$this->db->like('UserFullName', $name);
			}
			if($product_name!='')
			{
				$this->db->like('ProductName', $product_name);
				$this->db->or_like('ProductDescreption', $product_name);
				$this->db->or_like('UserDescription', $product_name);
			}
			$this->db->where('UserFlag',1);
			$query = $this->db->get();
			return $query->result_array();
		}
		if($a=='app')
		{
			$Token = $_POST['Token'];
			$SearchName = $_POST['SearchName'];
			$SearchCompany = $_POST['SearchCompany'];
			$SearchProduct = $_POST['SearchProduct'];
			$res=$this->user_model->TokenTimeExpirationCheck($Token);
			if($res==1)
			{
				$chatmsgresp['TokenFlag']=1;
				$chatmsgresp['ErrorFlag']=0;
	
				$this->db->select('PKUserID,UserType,UserFlag,UserMessageFlag,UserFullName,UserGender,UserHouseName,UserAddress,UserCity,UserPin,UserLandLine,UserMobile,UserEmail,UserDescription,UserName,UserImage,CompanyName,CompanyAddress,CompanyContact,ProductName,ProductDescreption');
				$this->db->from('user');
				$this->db->join("companies","user.UserCompanyName = companies.PKCompanyID");
				$this->db->join("products","user.UserService = products.PKProductID");
				if($SearchCompany!='')
				{
					$this->db->like('CompanyName', $SearchCompany);
				}
				if($SearchName!='')
				{
					$this->db->like('UserFullName', $SearchName);
				}
				if($SearchProduct!='')
				{
					$this->db->like('ProductName', $SearchProduct);
					$this->db->or_like('ProductDescreption', $SearchProduct);
					$this->db->or_like('UserDescription', $SearchProduct);
				}
				$this->db->where('UserFlag',1);
				$query = $this->db->get();
				$chatmsgresp['Directory'] = $query->result_array();
			}
			else if($res==2)
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=2;
				$chatmsgresp['Directory']="";
			}
			else if($res==0)
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=0;
				$chatmsgresp['Directory']="";
			}
			return $chatmsgresp;
		}
	}

}
?>