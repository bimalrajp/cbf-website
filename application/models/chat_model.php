<?php
class Chat_model extends CI_Model
{
	public function send_msg()
	{
		$MessageFile="";
		$Folder="";
		$TimeStamp=time();
		$MsgDate=date('d-m-Y',$TimeStamp);
		$MsgTime=date('h:i:s a',$TimeStamp);
		$DateTime=date('Y-m-d H:i:s',$TimeStamp);
		$Token = $_POST['Token'];
		$MessageType = $_POST['MessageType'];
		$res=$this->user_model->TokenTimeExpirationCheck($Token);
		if($res==1)
		{
			$UserTokenDetails=$this->user_model->get_Token_Details($Token);
			foreach($UserTokenDetails as $usertoken)
			{
				$FKUserID=$usertoken->FKUserID;
			}
			$UserDetails=$this->user_model->get_profile($FKUserID);
			foreach($UserDetails as $user)
			{
				$UserFullName = $user['UserFullName'];
				$UserMobile = $user['UserMobile'];
				$UserEmail = $user['UserEmail'];
				$UserCompanyName = $user['CompanyName'];
				$UserMessageFlag = $user['UserMessageFlag'];
			}
			if($UserMessageFlag==1)
			{
				$MsgCount=$this->db->count_all_results('messages')+1;
				$MessageText = $_POST['MessageText'];
				if($MessageType==2||$MessageType==3||$MessageType==4||$MessageType==5)
				{
					$MessageFileName = $_FILES['MessageFile']['name'];
					if($MessageFileName!="")
					{
						$extary=explode('.', $MessageFileName);
						$ext=array_pop($extary);
						$MessageFile = "MessageFile".$MsgCount.".".$ext;
						if($MessageType==2)
						{
							$Folder="images";
						}
						else if($MessageType==3)
						{
							$Folder="videos";
						}
						else if($MessageType==4)
						{
							$Folder="audios";
						}
						else if($MessageType==5)
						{
							$Folder="documents";
						}
						move_uploaded_file($_FILES["MessageFile"]["tmp_name"], "assets/website_assets/messages/".$Folder."/" .$MessageFile);
					}
					else
					{
						$MessageFile = "";
					}
				}
				$data=array(
					'FKUserID' => $FKUserID,
					'MessageText' => $MessageText,
					'MessageFile' => $MessageFile,
					'MessageType' => $MessageType,
					'MessageDateTime' => $DateTime
				);
				$r=$this->db->insert('messages', $data);
				if($r==1)
				{
					$insert_id = $this->db->insert_id();
					$chatmsgresp['ErrorFlag']=0;
					$chatmsgresp['TokenFlag']=1;
					$chatmsgresp['MsgSendFlag']=1;
					$chatmsgresp['MessageID']=$insert_id;
					$chatmsgresp['UserId']=$FKUserID;
					$chatmsgresp['UserFullName']=$UserFullName;
					$chatmsgresp['UserMobile']=$UserMobile;
					$chatmsgresp['UserEmail']=$UserEmail;
					$chatmsgresp['UserCompanyName']=$UserCompanyName;
					$chatmsgresp['MessageText']=$MessageText;
					if($MessageType!=1)
					{
						$chatmsgresp['MessageFile']=$MessageFile;
					}
					$chatmsgresp['MessageType']=$MessageType;
					$chatmsgresp['MsgDateTime']=$MsgDate." ".$MsgTime;
					$chatmsgresp['MessageDelFlag']=0;
				}
				else
				{
					$chatmsgresp['ErrorFlag']=1;
					$chatmsgresp['TokenFlag']=1;
					$chatmsgresp['MsgSendFlag']=0;
					$chatmsgresp['MessageID']="";
				}
				$chatmsgresp['UserMessageFlag'] =$UserMessageFlag;
			}
			else
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=1;
				$chatmsgresp['MsgSendFlag']=0;
				$chatmsgresp['UserMessageFlag'] =$UserMessageFlag;
			}
		}
		else if($res==2)
		{
			$chatmsgresp['ErrorFlag']=1;
			$chatmsgresp['TokenFlag']=2;
			$chatmsgresp['MsgSendFlag']=0;
			$chatmsgresp['UserMessageFlag'] ="";
		}
		else if($res==0)
		{
			$chatmsgresp['ErrorFlag']=1;
			$chatmsgresp['TokenFlag']=0;
			$chatmsgresp['MsgSendFlag']=0;
			$chatmsgresp['UserMessageFlag'] ="";
		}
		/*$fcm_path_url = "https://fcm.googleapis.com/fcm/send";
		$server_key = "AAAAhmVT5Yk:APA91bHzXP58QhyxNpV7WbnEPTm1TZ3hDmuSLrY9ptY0XdL5xj3Ghg1eQwdtcmsjm91u1c_0exnGwfxsIA3-lty9jP3rV4-jM6p5ncYjvPems0OQz8K6h1TDby8WQop7GUQPfecEEeXZ";
		$headers = array(
			'Authorization:key='.$server_key,
			'Content-Type:application/json'
		);*/
		return $chatmsgresp;
	}

	public function get_server_key()
	{
		$Token = $_POST['Token'];
		$res=$this->user_model->TokenTimeExpirationCheck($Token);
		if($res==1)
		{
			$server_key = "AAAAhmVT5Yk:APA91bHzXP58QhyxNpV7WbnEPTm1TZ3hDmuSLrY9ptY0XdL5xj3Ghg1eQwdtcmsjm91u1c_0exnGwfxsIA3-lty9jP3rV4-jM6p5ncYjvPems0OQz8K6h1TDby8WQop7GUQPfecEEeXZ";
			$chatmsgresp['FB_CM_Server_Key']=urlencode($server_key);
		}
		else
		{
			$chatmsgresp['FB_CM_Server_Key']="";
		}
		return $chatmsgresp;
	}

	public function get_msgs()
	{
		$limit=20;
		$end=0;
		$Token = $_POST['Token'];
		$MaxID = $_POST['MaxID'];
		$MinID = $_POST['MinID'];
		$UpDown = $_POST['UpDown'];
		$res=$this->user_model->TokenTimeExpirationCheck($Token);
		if($res==1)
		{
			$UserTokenDetails=$this->user_model->get_Token_Details($Token);
			foreach($UserTokenDetails as $usertoken)
			{
				$FKUserID=$usertoken->FKUserID;
			}
			$UserDetails=$this->user_model->get_profile($FKUserID);
			foreach($UserDetails as $user)
			{
				$UserMessageFlag = $user['UserMessageFlag'];
			}
			
			if($UserMessageFlag == 1)
			{
			
				$chatmsgresp['ErrorFlag']=0;
				$chatmsgresp['TokenFlag']=1;

				$this->db->select('PKMessageID,FKUserID,MessageText,MessageFile,MessageType,MessageDateTime,MessageDelFlag,UserFullName,UserMobile,UserEmail,CompanyName');
				$this->db->from('messages');
				if($UpDown==1)
				{
					$this->db->where('PKMessageID >',$MaxID);
				}
				if($MinID!=0 && $UpDown==0)
				{
					$this->db->where('PKMessageID <',$MinID);
				}
				if($UpDown==0 || ($UpDown==1 && $MaxID==0))
				{
					$this->db->limit($limit);
				}
				$this->db->order_by("PKMessageID","desc");
				$this->db->join('user', 'messages.FKUserID = user.PKUserID','left');
				$this->db->join('companies', 'user.UserCompanyName = companies.PKCompanyID','left');
				$query = $this->db->get();
				$chatmsgresp['MessagesCount']=$query->num_rows();
				if($chatmsgresp['MessagesCount']>0)
				{
					$i=0;
					foreach($query->result_array() as $message)
					{
						$chatmsgresp['Messages'][$i]['MessageID'] = $message["PKMessageID"];
						$chatmsgresp['Messages'][$i]['UserId'] = $message["FKUserID"];
						$chatmsgresp['Messages'][$i]['UserFullName'] = $message["UserFullName"];
						$chatmsgresp['Messages'][$i]['UserMobile'] = $message["UserMobile"];
						$chatmsgresp['Messages'][$i]['UserEmail'] = $message["UserEmail"];
						$chatmsgresp['Messages'][$i]['UserCompanyName'] = $message["CompanyName"];
						$chatmsgresp['Messages'][$i]['MessageText'] = $message["MessageText"];
						$chatmsgresp['Messages'][$i]['MessageFile'] = $message["MessageFile"];
						$chatmsgresp['Messages'][$i]['MessageType'] = $message["MessageType"];
						$chatmsgresp['Messages'][$i]['MsgDateTime'] = $message["MessageDateTime"];
						$chatmsgresp['Messages'][$i]['MessageDelFlag'] = $message["MessageDelFlag"];
						$i++;
					}
				}
				else
				{
					//$chatmsgresp['Messages']="[]";
				}
				$chatmsgresp['UserMessageFlag'] = $UserMessageFlag;
			}
			else
			{
				$chatmsgresp['ErrorFlag']=1;
				$chatmsgresp['TokenFlag']=1;
				$chatmsgresp['MessagesCount']=0;
				$chatmsgresp['UserMessageFlag'] = $UserMessageFlag;
			}
		}
		else if($res==2)
		{
			$chatmsgresp['ErrorFlag']=1;
			$chatmsgresp['TokenFlag']=2;
			$chatmsgresp['MessagesCount']=0;
		}
		else if($res==0)
		{
			$chatmsgresp['ErrorFlag']=1;
			$chatmsgresp['TokenFlag']=0;
			$chatmsgresp['MessagesCount']=0;
		}
		return $chatmsgresp;
	}

	public function get_directory()
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('companies', 'user.UserCompanyName =companies.PKCompanyID','left');
		$this->db->join('products', 'user.UserService =products.PKProductID','left');
		$this->db->where('UserType',0);
		$this->db->where('UserFlag',1);
		$this->db->order_by("UserFullName","asc");
		$query = $this->db->get();
		return $query->result_array();
	}

	public function update_profile($id='')
	{
		$id=$_SESSION['PKUserID'];
		$photo=$this->input->post('photo');
		$filename=$_FILES['photo']['name'];
		if($filename!="")
		{
			$ext=array_pop(explode('.', $filename));
			$word = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
			shuffle($word);
			$filename1= substr(implode($word), 0, 10);
			//$posttimemod=preg_replace("[:]", "", $posttime);
			$photo = $filename1.'.'.$ext;
			move_uploaded_file($_FILES["photo"]["tmp_name"], "assets/user/" .$photo);
		}
		if($filename =="")
		{
			$photo=$this->input->post('photo1');
		}
		$companyid=$this->input->post('UserCompanyID');
		$companyname=$this->input->post('UserCompanyName');
		$datacompany = array(
			'CompanyName' => $companyname
		);
		$this->db->where('PKCompanyID', $companyid);
		$this->db->update('companies', $datacompany);
		$serviceid=$this->input->post('UserServiceID');
		$service=$this->input->post('UserService');
		$dataservice = array(
			'ProductName' => $service
		);
		$this->db->where('PKProductID', $serviceid);
		$this->db->update('products', $dataservice);
		$data = array(
			'UserImage' => $photo,
			'UserFullName'=>$this->input->post('UserFullName'),
			'UserHouseName'=>$this->input->post('UserHouseName'),
			'UserGender'=>$this->input->post('UserGender'),
			'UserAddress'=>$this->input->post('UserAddress'),
			'UserCity'=>$this->input->post('UserCity'),
			'UserPin'=>$this->input->post('UserPin'),
			'UserLandLine'=>$this->input->post('UserLandLine'),
			'UserMobile'=>$this->input->post('UserMobile'),
			'UserEmail'=>$this->input->post('UserEmail'),
			'UserDescription'=>$this->input->post('UserDescription')
		);
		$this->db->where('PKUserID', $id);
		return $this->db->update('user', $data);
	}

	function Token($UID,$UN,$logintype)
	{
		$res='';
		$TokenType=0;
		$LoginTime=date('Y-m-d H:i:s');
		$Token=urlencode($this->encrypt->encode($UID.$UN.$LoginTime));

		if($logintype=='web')
		{
			$TokenType=1;
		}
		else if($logintype=='app')
		{
			$TokenType=2;
		}
		$data = array
		(
			'FKUserID' => $UID,
			'Token' => $Token,
			'TokenType' => $TokenType,
			'LoginFlag' => 1,
			'LoginTime' => $LoginTime,
			'LastActiveTime' => $LoginTime
		);
		$res=$this->db->insert('logintokens', $data);

		if($res==1)
		{
			$this->db->select('*');
			$this->db->from('logintokens');
			$this->db->where('Token', $Token);
			$this->db->limit(1);
			$query=$this->db->get();
			return $query->result();
		}
	}

	function TokenTimeExpirationCheck($Token)
	{
		$ExpirationDays=30;
		$ExpirationSecs=5;
		$UpdatedOn=date('Y-m-d H:i:s');
		$this->db->select('LastActiveTime');
		$this->db->from('logintokens');
		$this->db->where('Token', $Token);
		$this->db->where('LoginFlag', 1);
		$this->db->limit(1);
		$query=$this->db->get();

		if($query->num_rows()==1)
		{
			foreach($query->result() as $row)
			{
				$LastTime=$row->LastActiveTime;
			}
			/*$TimeDifference=round(abs(strtotime($UpdatedOn) - strtotime($LastTime))/86400,2);*/
			$TimeDifference=intval(abs(strtotime($UpdatedOn) - strtotime($LastTime))/86400);
			//$TimeDifference=intval(abs(strtotime($UpdatedOn) - strtotime($LastTime)));

			if($TimeDifference<=$ExpirationDays)
			{
				$data = array
				(
					'LastActiveTime' => $UpdatedOn
				);
				$this->db->where('Token', $Token);
				$this->db->update('logintokens', $data);
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 2;
		}
	}

	function TokenChangeOnLogout($Token)
	{
		$AutoLogout=0;
		$UpdatedOn=date('Y-m-d H:i:s');
		$TTE=$this->TokenTimeExpirationCheck($Token);
		if($TTE==1||$TTE==0)
		{
			if($TTE==0)
			{
				$AutoLogout=1;
			}
			$data = array
			(
				'LogoutTime' => $UpdatedOn,
				'LoginFlag' => 0,
				'AutoLogout' => $AutoLogout
			);
			$this->db->where('Token', $Token);
			$r=$this->db->update('logintokens', $data);
			if($r==true)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 2;
		}
	}
}
?>