<?php
session_start();
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Home_Controller extends CI_Controller
{
	function __construct()
 	{
   		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		date_default_timezone_set('asia/kolkata');
	}

	function verifylogin($a='web', $from_register=0)
	{
		$_SESSION["messagetype"]=1;
		if($from_register==0)
		{
			$username = $this->input->post('Username');
			$password = $this->input->post('Password');
		}
		else if($from_register==1)
		{
			$username = $_POST["RUserName"];
			$password = $_POST["RPassword"];
		}
		
		$logindetails=$this->check_database($username,$password,$a);
		
		if($logindetails==2)
		{
			if($a=='web')
			{
				$_SESSION["message"]="INCORRECT PASSWORD!!";
				$_SESSION["messagetype"]=0;
				redirect(base_url(), 'refresh');
			}
			else if($a=='app')
			{
				$siteresponse['LoginFlag']=$logindetails;
				$siteresponse['ErrorFlag']=0;
				echo json_encode($siteresponse);
			}
		}
		else if($logindetails==3)
		{
			if($a=='web')
			{
				$_SESSION["message"]="INCORRECT USER CREDENTIALS!!";
				$_SESSION["messagetype"]=0;
				redirect(base_url(), 'refresh');
			}
			else if($a=='app')
			{
				$siteresponse['LoginFlag']=$logindetails;
				$siteresponse['ErrorFlag']=0;
				echo json_encode($siteresponse);
			}
		}
		else
		{
			if($a=='web')
			{
				$_SESSION["PKUserID"]=$logindetails['PKUserID'];
				$_SESSION["UserFullName"]=$logindetails['UserFullName'];
				$_SESSION["Token"]=$logindetails['Token'];
				$_SESSION["UserType"]=$logindetails['LoginType'];

				if($logindetails['Status']==1)
				{
					if($logindetails['LoginType']==1)
					{
						redirect(base_url().'admin/home/web', 'refresh');
					}
					if($logindetails['LoginType']==0)
					{
						redirect(base_url().'user/home/web', 'refresh');
					}
				}
				else if($logindetails['Status']==0)
				{
					$_SESSION["message"]="Your account is in TEMPORARILY INACTIVE STATE!!";
					$_SESSION["messagetype"]=0;
					redirect(base_url(), 'refresh');
				}
			}
			else if($a=='app')
			{
				if($from_register == 1)
				{
					$logindetails['RegisterFlag']=1;
				}
				$logindetails['ErrorFlag']=0;
				echo $data['siteresponse']=json_encode($logindetails);
			}
		}
	}


	function check_database($username,$password,$a)
	{
		$result = $this->user_model->Login($username, $password);
		//echo $this->encrypt->encode('admin');
		if($result!=2 && $result!=3)
		{
			$loginresult = $result;
			/*foreach($loginresult as $row)
			{
				$logindetails['PKUserID']=$row->PKUserID;
				$logindetails['UserFullName']=$row->UserFullName;
				$logindetails['UserType']=$row->UserType;
			}*/
			foreach($loginresult as $row)
			{
				$logindetails['Status']=$row->UserFlag;
				$logindetails['MessageFlag']=$row->UserMessageFlag;
				$logindetails['LoginType']=$row->UserType;
				if($logindetails['Status']==1)
				{
					$logindetails['PKUserID']=$row->PKUserID;
					$logindetails['UserFullName']=$row->UserFullName;
					$logindetails['UserName']=$row->UserName;
					$CreatedToken = $this->user_model->Token($logindetails['PKUserID'],$logindetails['UserName'],$a,$logindetails['LoginType']);
					foreach($CreatedToken as $row1)
					{
						$logindetails['Token']=$row1->Token;
						$logindetails['LoginTime']=$row1->LoginTime;
					}
				}
				else
				{
					$logindetails['PKUserID']=$row->PKUserID;
					$logindetails['UserFullName']="";
					$logindetails['UserName']="";
					$logindetails['Token']="";
					$logindetails['LoginTime']="";
				}
			}
			$logindetails['LoginFlag']=1;
			return $logindetails;
		}
		else
		{
			return $result;
		}
	}
	
	function logout($a="")
	{
		/*session_unset();
		//session_destroy();
		$_SESSION['message']="Logged Out Successfully";
		redirect(base_url(), 'refresh');*/
		if($a=="web")
		{
			if($_SESSION['UserType']==0)
			{
				$Token=$_SESSION['Token'];
				$res=$this->user_model->TokenChangeOnLogout($Token);
			}
			else
			{
				$res=1;
			}
			session_unset();
			if($res==0)
			{
				$_SESSION['message']="Logged Out due to Inactivity!! Please Login Again!!";
			}
			else if($res==1)
			{
				$_SESSION['message']="Logged Out Successfully";
			}
			else if($res==2)
			{
				$_SESSION['message']="Incorrect Parameters!! Logged Out!!";
			}
			redirect(base_url(), 'refresh');
		}
		else if($a=="app")
		{
			$Token=$_POST['Token'];
			$res=$this->user_model->TokenChangeOnLogout($Token);
			$siteresponse['LogoutStatus']=$res;
			if($res==0)
			{
				$siteresponse['LogoutMessage']="Logged Out due to Inactivity!! Please Login Again!!";
			}
			else if($res==1)
			{
				$siteresponse['LogoutMessage']="Logged Out Successfully";
			}
			else if($res==2)
			{
				$siteresponse['LogoutMessage']="Incorrect Parameters!! Logged Out!!";
			}
			$siteresponse['Error']=0;
			echo json_encode($siteresponse);
		}
	}
}