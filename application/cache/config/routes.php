<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['post/(:any)'] = "post/view/$1";
$route['post/presult'] = "post/presult";
$route['post/search'] = "post/search";
$route['post/result'] = "post/result";
$route['post'] = "post";
$route['admin/post/(:any)'] = "admin/post/$1";
$route['admin/post'] = "admin/post";
$route['category/(:any)'] = "category/view/$1";
$route['category'] = "category";
$route['admin/location/(:any)'] = "admin/location/$1";
$route['admin/location'] = "admin/location";
$route['admin/category/(:any)'] = "admin/category/$1";
$route['admin/category'] = "admin/category";
$route['admin/(:any)'] = "admin/admin/$1";
$route['admin'] = "admin/admin";
$route['ajax/get_subcat/(:any)'] = 'ajax/get_subcat/$1';
$route['ajax'] = 'ajax';
$route['default_controller'] = "page";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */